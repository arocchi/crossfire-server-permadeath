/**
 * @file
 * Artifact-related structures.
 *
 * See the @ref page_artifact "page on artifacts" for more information.
 */

#ifndef ARTIFACT_H
#define ARTIFACT_H

/**
 * This is one artifact, ie one special item.
 */
struct artifact {
    object *item;                   /**< Special values of the artifact. Note that this object is malloc() ed. */
    uint16_t chance;                  /**< Chance of the artifact to happen. */
    uint8_t difficulty;               /**< Minimum map difficulty for the artifact to happen. */
    std::vector<sstring> allowed;   /**< List of archetypes the artifact can affect. */
};

/**
 * This represents all archetypes for one particular object type.
 */
struct artifactlist {
    uint8_t type;                         /**< Object type that this list represents. */
    uint16_t total_chance;                /**< Sum of chance for are artifacts on this list. */
    artifactlist *next;    /**< Next list of artifacts. */
    std::vector<artifact *> items;      /**< Artifacts for this type. Will never be NULL. */
};

#endif /* ARTIFACT_H */
