# 2023-02-05

Add **auto-completion suggestions** for resource filters, in both quick filter and filter dialog.

# 2022-07-15

**Implement quest creation**, right-click on a quest and select *Add quest*.

# 2022-05-30

**Rework of quests panels**.

# 2022-05-13

**Display of attack messages**, by attack type. When selecting an item, the message for the hitter and the victim as presented in-game is displayed.

# 2022-02-18

**Graphical overview of monster resistances** to fire, cold, electricity, poison, in *Tools* menu.

# 2022-01-30

**Option to restore windows positions** when application starts, in *Windows* menu.

# 2022-01-28

**Display artifacts** for archetypes and treasure items, with their associated generation chance.

# 2021-12-30

**Basic help**, contextual - not many topics for now, but it may come.

# 2021-12-16

**Treasure list edition**, with drag and drop of archetype or treasure list to add items to a treasure list, right-click on items to remove or change yes/no.

# 2021-12-10
**Changelog**: CRE will by default display changes when started for the first time after an upgrade. This can be disabled in the "Help" menu.

# 2021-12-09

**General messages edition**, the identifier can't be changed, and no possibility to create/delete messages.
