/*
 * Crossfire -- cooperative multi-player graphical RPG and adventure game
 *
 * Copyright (c) 2022 the Crossfire Development Team
 *
 * Crossfire is free software and comes with ABSOLUTELY NO WARRANTY. You are
 * welcome to redistribute it under certain conditions. For details, please
 * see COPYING and LICENSE.
 *
 * The authors can be reached via e-mail at <crossfire@metalforge.org>.
 */

#ifndef _CREEXPERIENCEWINDOW_H
#define _CREEXPERIENCEWINDOW_H

#include <QWidget>

class CREExperienceWindow : public QWidget
{
    Q_OBJECT
    public:
        CREExperienceWindow();
        virtual ~CREExperienceWindow();
};

#endif /* _CREEXPERIENCEWINDOW_H */
