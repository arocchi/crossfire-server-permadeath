/*
 * Crossfire -- cooperative multi-player graphical RPG and adventure game
 *
 * Copyright (c) 2022 the Crossfire Development Team
 *
 * Crossfire is free software and comes with ABSOLUTELY NO WARRANTY. You are
 * welcome to redistribute it under certain conditions. For details, please
 * see COPYING and LICENSE.
 *
 * The authors can be reached via e-mail at <crossfire@metalforge.org>.
 */

#include "HelpBrowser.h"
#include <QHelpEngine>

HelpBrowser::HelpBrowser(QHelpEngine* helpEngine, QWidget* parent) : QTextBrowser(parent), helpEngine(helpEngine) {
}

QVariant HelpBrowser::loadResource(int, const QUrl &name) {
    if (name.scheme() == "qthelp") {
        return QVariant(helpEngine->fileData(name));
    }
    return QVariant();
}
