#ifndef _CRE_PCH_H
#define _CRE_PCH_H

#include <stdbool.h>
#include "global.h"
#include "artifact.h"
#include "face.h"
#include "image.h"
#include "map.h"
#include "object.h"
#include "treasure.h"

#include <QObject>
#include <Qt>
#include <QWidget>
#include <QtWidgets>
#include <QList>
#include <QHash>
#include <QStringList>


#endif /* _CRE_PCH_H */
