<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AllAssets</name>
    <message>
        <location filename="../AllAssets.cpp" line="46"/>
        <source>Display all facesets.</source>
        <translation>Afficher tous les assortiments d&apos;images.</translation>
    </message>
    <message>
        <location filename="../AllAssets.cpp" line="46"/>
        <source>Facesets</source>
        <translation>Assortiments d&apos;images</translation>
    </message>
    <message>
        <location filename="../AllAssets.h" line="28"/>
        <source>All assets</source>
        <translation>Toutes les ressources</translation>
    </message>
</context>
<context>
    <name>AnimationPanel</name>
    <message>
        <location filename="../animations/AnimationPanel.cpp" line="32"/>
        <source>Used by</source>
        <translation>Utilisé par</translation>
    </message>
    <message>
        <location filename="../animations/AnimationPanel.cpp" line="36"/>
        <source>Faces</source>
        <translation>Images</translation>
    </message>
</context>
<context>
    <name>AnimationsWrapper</name>
    <message>
        <location filename="../animations/AnimationsWrapper.h" line="32"/>
        <source>Display all animations.</source>
        <translation>Afficher toutes les animations.</translation>
    </message>
    <message>
        <location filename="../animations/AnimationsWrapper.h" line="32"/>
        <source>Animations</source>
        <translation>Animations</translation>
    </message>
</context>
<context>
    <name>ArchetypeComboBox</name>
    <message>
        <location filename="../archetypes/ArchetypeComboBox.cpp" line="29"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>ArchetypePanel</name>
    <message>
        <location filename="../archetypes/ArchetypePanel.cpp" line="16"/>
        <source>Raw archetype:</source>
        <translation>Archétype brut :</translation>
    </message>
    <message>
        <location filename="../archetypes/ArchetypePanel.cpp" line="17"/>
        <source>Used by</source>
        <translation>Utilisé par</translation>
    </message>
</context>
<context>
    <name>ArchetypesWrapper</name>
    <message>
        <location filename="../archetypes/ArchetypesWrapper.cpp" line="27"/>
        <source>Display all archetypes.</source>
        <translation>Afficher tous les archétypes.</translation>
    </message>
    <message>
        <location filename="../archetypes/ArchetypesWrapper.h" line="29"/>
        <source>Archetypes</source>
        <translation>Archétypes</translation>
    </message>
</context>
<context>
    <name>ArtifactListWrapper</name>
    <message>
        <location filename="../artifacts/ArtifactListWrapper.h" line="32"/>
        <source>type %1</source>
        <translation>type %1</translation>
    </message>
</context>
<context>
    <name>ArtifactPanel</name>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="27"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="28"/>
        <source>Chance:</source>
        <translation>Chance :</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="29"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="30"/>
        <source>File:</source>
        <translation>Fichier :</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="35"/>
        <source>Values:</source>
        <translation>Valeurs :</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="38"/>
        <source>Allowed/forbidden archetypes</source>
        <translation>Archétypes autorisés / interdits</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="43"/>
        <source>Result:</source>
        <translation>Résultat :</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="86"/>
        <source>Can&apos;t be made via alchemy.</source>
        <translation>Ne peut être généré par alchimie.</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="90"/>
        <source>Can be made via alchemy.</source>
        <translation>Peut être généré par alchimie.</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="94"/>
        <source>The following archetypes can be used via alchemy: %1</source>
        <translation>Les archétypes suivants peuvent être utilisé pour de l&apos;alchimie : %1</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactPanel.cpp" line="94"/>
        <source>, </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ArtifactWrapper</name>
    <message>
        <location filename="../artifacts/ArtifactWrapper.cpp" line="29"/>
        <location filename="../artifacts/ArtifactWrapper.cpp" line="37"/>
        <source>%1 (%2%, %3 chances on %4)</source>
        <translation>%1 (%2%, %3 chances sur %4)</translation>
    </message>
</context>
<context>
    <name>ArtifactsWrapper</name>
    <message>
        <location filename="../artifacts/ArtifactsWrapper.cpp" line="20"/>
        <source>Display all artifacts.</source>
        <translation>Afficher tous les artéfacts.</translation>
    </message>
    <message>
        <location filename="../artifacts/ArtifactsWrapper.h" line="30"/>
        <source>Artifacts</source>
        <translation>Artéfacts</translation>
    </message>
</context>
<context>
    <name>AssetModel</name>
    <message>
        <location filename="../assets/AssetModel.cpp" line="88"/>
        <source>Asset</source>
        <translation>Ressource</translation>
    </message>
</context>
<context>
    <name>AssetOriginAndCreationDialog</name>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="17"/>
        <source>Quest creation</source>
        <translation>Création de quête</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="18"/>
        <source>Treasure creation</source>
        <translation>Création de trésor</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="21"/>
        <source>Quest file definition</source>
        <translation>Fichier de définition de quête</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="22"/>
        <source>Treasure list file definition</source>
        <translation>Fichier de définition de trésor</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="25"/>
        <source>quest</source>
        <translation>quête</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="26"/>
        <source>treasure</source>
        <translation>trésor</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="45"/>
        <source>Code:</source>
        <translation>Code :</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="52"/>
        <source>Add to an existing %1 file:</source>
        <translation>Ajouter à un fichier de %1 existant :</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="60"/>
        <source>Create a new %1 file:</source>
        <translation>Créer un nouveau fichier de %1 :</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="64"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="73"/>
        <source>%1 file (*%2);;All files (*.*)</source>
        <translation>fichier %1 (*%2);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="105"/>
        <source>Empty required field</source>
        <translation>Champ requis vide</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="105"/>
        <source>Please enter a code.</source>
        <translation>Veuillez entrer un code.</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="110"/>
        <source>Code already exists</source>
        <translation>Ce code existe déjà</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="110"/>
        <source>The code you entered matches an existing code.</source>
        <translation>Le code que vous avez choisi correspond à un code existant.</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="117"/>
        <source>Empty file</source>
        <translation>Fichier vide</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="117"/>
        <source>Please enter a file to define the %1 into.</source>
        <translation>Veuillez définir un fichier dans lequel définir le %1.</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="122"/>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="128"/>
        <source>File warning</source>
        <translation>Avertissement de fichier</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="122"/>
        <source>The file seems to be outside the root directory for this kind of assets (%1).
This means the asset will not be visible to the game.
Are you sure you want to use this file?</source>
        <translation>Le fichier semble être situé hors du répertoire racine pour ce type de ressources (%1).
Ceci signifie que la ressource ne sera pas disponible pour le jeu.
Confirmez-vous vouloir utiliser de fichier ?</translation>
    </message>
    <message>
        <location filename="../assets/AssetOriginAndCreationDialog.cpp" line="128"/>
        <source>The file does not end with &apos;%1&apos;.
This means the asset will not be visible to the game.
Are you sure you want to use this file?</source>
        <translation>Le fichier ne se termine pas par « %1 ».
Ceci signifie que la ressource ne sera pas disponible pour le jeu.
Confirmez-vous vouloir utiliser de fichier ?</translation>
    </message>
</context>
<context>
    <name>AttackMessagePanel</name>
    <message>
        <location filename="../attack_messages/AttackMessagePanel.cpp" line="22"/>
        <source>Max damage</source>
        <translation>Dégâts maximum</translation>
    </message>
    <message>
        <location filename="../attack_messages/AttackMessagePanel.cpp" line="22"/>
        <source>First part</source>
        <translation>Première partie</translation>
    </message>
    <message>
        <location filename="../attack_messages/AttackMessagePanel.cpp" line="22"/>
        <source>Second part</source>
        <translation>Second partie</translation>
    </message>
    <message>
        <location filename="../attack_messages/AttackMessagePanel.cpp" line="22"/>
        <source>For victim</source>
        <translation>Pour la victime</translation>
    </message>
</context>
<context>
    <name>AttackMessagesWrapper</name>
    <message>
        <location filename="../attack_messages/AttackMessagesWrapper.h" line="37"/>
        <source>Attack messages</source>
        <translation>Messages d&apos;attaque</translation>
    </message>
    <message>
        <location filename="../attack_messages/AttackMessagesWrapper.cpp" line="40"/>
        <source>Display all attack messages.</source>
        <translation>Affiche tous les messages d&apos;attaque.</translation>
    </message>
</context>
<context>
    <name>CRECombatSimulator</name>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="27"/>
        <source>First fighter:</source>
        <translation>Premier combattant :</translation>
    </message>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="31"/>
        <source>Second fighter:</source>
        <translation>Second combattant :</translation>
    </message>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="35"/>
        <source>Number of fights:</source>
        <translation>Nombre de combats :</translation>
    </message>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="41"/>
        <source>Maximum number of rounds:</source>
        <translation>Nombre maximum de rounds :</translation>
    </message>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="48"/>
        <source>Combat result:</source>
        <translation>Résultat du combat :</translation>
    </message>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="60"/>
        <source>Combat simulator</source>
        <translation>Simulateur de combat</translation>
    </message>
    <message>
        <location filename="../CRECombatSimulator.cpp" line="175"/>
        <source>Draw: %1 fights
%2 victories: %3 (max hp: %4, min hp: %5)
%6 victories: %7 (max hp: %8, min hp: %9)</source>
        <translation>Matchs nuls : %1 combats
%2 victoires : %3 (points de vie maximum : %4, points de vie minimum : %5)
%6 victoires : %7 (points de vie maximum : %8, points de vie minimum : %9)</translation>
    </message>
</context>
<context>
    <name>CREExperienceWindow</name>
    <message>
        <location filename="../CREExperienceWindow.cpp" line="28"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <location filename="../CREExperienceWindow.cpp" line="28"/>
        <location filename="../CREExperienceWindow.cpp" line="45"/>
        <source>Experience</source>
        <translation>Expérience</translation>
    </message>
    <message>
        <location filename="../CREExperienceWindow.cpp" line="28"/>
        <source>Difference</source>
        <translation>Différence</translation>
    </message>
</context>
<context>
    <name>CREFacePanel</name>
    <message>
        <location filename="../faces/FacePanel.cpp" line="33"/>
        <source>License field</source>
        <translation>Champ de licence</translation>
    </message>
    <message>
        <location filename="../faces/FacePanel.cpp" line="33"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../faces/FacePanel.cpp" line="39"/>
        <source>Magicmap color: </source>
        <translation>Couleur sur la carte magique :</translation>
    </message>
    <message>
        <location filename="../faces/FacePanel.cpp" line="48"/>
        <source>Face file: </source>
        <translation>Fichier de définition :</translation>
    </message>
    <message>
        <location filename="../faces/FacePanel.cpp" line="51"/>
        <source>Save face</source>
        <translation>Sauvegarder l&apos;image</translation>
    </message>
    <message>
        <location filename="../faces/FacePanel.cpp" line="55"/>
        <source>Make smooth base</source>
        <translation>Générer une base de lissage</translation>
    </message>
</context>
<context>
    <name>CREFilterDialog</name>
    <message>
        <location filename="../CREFilterDialog.cpp" line="25"/>
        <source>Filter parameters</source>
        <translation>Paramètres de filtres</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="32"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="36"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="40"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="45"/>
        <source>Filter:</source>
        <translation>Filtre :</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="72"/>
        <source>Discard changes?</source>
        <translation>Abandonner les modifications ?</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="72"/>
        <source>You are about to discard all changes!
Are you sure?</source>
        <translation>Vous allez abandonner toutes les modifications !
Confirmez-vous ?</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="80"/>
        <source>Filter help</source>
        <translation>Aide de filtre</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="80"/>
        <source>Enter the script expression with which to filter items in the view. Current item is &lt;b&gt;item&lt;/b&gt;, and it has the following properties:&lt;br /&gt;&lt;ul&gt;&lt;li&gt;for an archetype: name, clone, head, more&lt;ul&gt;&lt;li&gt;clone has the following properties: name, race, type, level, isMonster, isAlive, experience, attacktype, ac, wc, &lt;/li&gt;&lt;li&gt;head and more are archetypes if not null&lt;/li&gt;&lt;/ul&gt;&lt;/li&gt;&lt;li&gt;for a formulae: title, chance, difficulty, archs&lt;/li&gt;&lt;li&gt;for an artifact: item, chance, difficulty, allowed&lt;/li&gt;&lt;/ul&gt;&lt;br /&gt;An item is shown if the expression evaluates to &lt;i&gt;true&lt;/i&gt;. If a property is not defined for the current item, it will not be shown.&lt;br /&gt;&lt;br /&gt;Examples:&lt;ul&gt;&lt;li&gt;archetypes of type 5: &lt;i&gt;item.clone.type == 5&lt;/i&gt;&lt;/li&gt;&lt;li&gt;artifact allowed for all items of the type: &lt;i&gt;item.allowed.length == 0&lt;/i&gt;&lt;/il&gt;&lt;/ul&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="87"/>
        <source>&lt;new filter&gt;</source>
        <translation>&lt;nouveau filtre&gt;</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="99"/>
        <source>Delete filter?</source>
        <translation>Supprimer le filtre ?</translation>
    </message>
    <message>
        <location filename="../CREFilterDialog.cpp" line="99"/>
        <source>Really delete filter &apos;%1&apos;?</source>
        <translation>Souhaitez-vous réellement supprimer le filtre « %1 » ?</translation>
    </message>
</context>
<context>
    <name>CREGeneralMessagePanel</name>
    <message>
        <location filename="../general_messages/GeneralMessagePanel.cpp" line="16"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../general_messages/GeneralMessagePanel.cpp" line="17"/>
        <source>Identifier:</source>
        <translation>Identifiant :</translation>
    </message>
    <message>
        <location filename="../general_messages/GeneralMessagePanel.cpp" line="18"/>
        <source>Quest:</source>
        <translation>Quête :</translation>
    </message>
    <message>
        <location filename="../general_messages/GeneralMessagePanel.cpp" line="19"/>
        <source>Chance:</source>
        <translation>Chance :</translation>
    </message>
    <message>
        <location filename="../general_messages/GeneralMessagePanel.cpp" line="20"/>
        <source>Face:</source>
        <translation>Image :</translation>
    </message>
    <message>
        <location filename="../general_messages/GeneralMessagePanel.cpp" line="21"/>
        <source>Message:</source>
        <translation>Message :</translation>
    </message>
</context>
<context>
    <name>CREHPBarMaker</name>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="26"/>
        <source>Path where to create items:</source>
        <translation>Chemin dans lequel écrire les fichiers générés :</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="30"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="34"/>
        <source>Archetype name:</source>
        <translation>Nom d&apos;archétype :</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="38"/>
        <source>Color:</source>
        <translation>Couleur :</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="43"/>
        <source>Y position from top:</source>
        <translation>Position y à partir du haut :</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="48"/>
        <source>Bar height:</source>
        <translation>Hauteur de la barre :</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="62"/>
        <source>HP bar face generator</source>
        <translation>Générateur d&apos;images pour barre de points de vie</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="73"/>
        <location filename="../CREHPBarMaker.cpp" line="79"/>
        <source>Oops</source>
        <translation>Oups</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="73"/>
        <source>You must select a destination!</source>
        <translation>Vous devez choisir une destination !</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="79"/>
        <source>You must enter a name!</source>
        <translation>Vous devez entrer un nom d&apos;archétype !</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="87"/>
        <source>Confirm file overwrite</source>
        <translation>Confirmez l&apos;écrasement du fichier</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="87"/>
        <source>File %1 already exists. Overwrite it?</source>
        <translation>Le fichier %1 existe déjà. L&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="94"/>
        <location filename="../CREHPBarMaker.cpp" line="115"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="94"/>
        <source>Error while opening the archetype file %1!</source>
        <translation>Erreur lors de l&apos;ouverture du fichier d&apos;archétype %1 !</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="115"/>
        <source>Error while saving the picture %1!</source>
        <translation>Erreur lors de l&apos;écriture de l&apos;image %1 !</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="123"/>
        <source>Bar created</source>
        <translation>Barre créée</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="123"/>
        <source>The bar was correctly saved as %1</source>
        <translation>La barre de points de vie a été sauvegardée sous le nom %1</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="128"/>
        <source>Select destination directory</source>
        <translation>Sélectionnez le répertoire de destination</translation>
    </message>
    <message>
        <location filename="../CREHPBarMaker.cpp" line="143"/>
        <source>Select bar color</source>
        <translation>Sélectionnez la couleur de la barre</translation>
    </message>
</context>
<context>
    <name>CREMainWindow</name>
    <message>
        <location filename="../CREMainWindow.cpp" line="82"/>
        <source>Ready</source>
        <translation>Prêt</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="83"/>
        <source>Browsing maps...</source>
        <translation>Parcours des cartes...</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="86"/>
        <location filename="../CREMainWindow.cpp" line="298"/>
        <location filename="../CREMainWindow.cpp" line="1818"/>
        <source>Crossfire Resource Editor</source>
        <translation>Éditeur de Ressources Crossfire</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="165"/>
        <source>Formulae</source>
        <translation>Formules</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="169"/>
        <source>Player vs monsters</source>
        <translation>Joueur contre monstres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="170"/>
        <source>Compute statistics related to player vs monster combat.</source>
        <translation>Calcule des statistiques liées au combat joueur contre monstre.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="175"/>
        <source>Shop specialization</source>
        <translation>Spécialisation des boutiques</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="176"/>
        <source>Display the list of shops and their specialization for items.</source>
        <translation>Affiche la liste des boutiques et leur spécialisation par rapport aux objets.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="181"/>
        <source>Quest solved by players</source>
        <translation>Quêtes résolues par les joueurs</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="182"/>
        <source>Display quests the players have solved.</source>
        <translation>Affiche les quêtes terminées par les joueurs.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="187"/>
        <source>Unused archetypes</source>
        <translation>Archétypes inutilisés</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="188"/>
        <source>Display all archetypes which seem unused.</source>
        <translation>Affiche tous les archétypes qui semblent non utilisés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="192"/>
        <source>Clear map cache</source>
        <translation>Effacer le cache des cartes</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="193"/>
        <source>Force a refresh of all map information at next start.</source>
        <translation>Force une analyse de toutes les cartes au prochain démarrage de l&apos;application.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="198"/>
        <source>Use set fallback for missing faces</source>
        <translation>Utiliser l&apos;alternative de l&apos;assortiment pour les images non définies</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="206"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="215"/>
        <source>Display all defined assets, except the experience table.</source>
        <translation>Affiche toutes les ressources définies, à l&apos;exception de l&apos;expérience.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="215"/>
        <source>Assets</source>
        <translation>Ressources</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="220"/>
        <source>Experience</source>
        <translation>Expérience</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="220"/>
        <source>Display the experience table.</source>
        <translation>Affiche la table d&apos;expérience.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="223"/>
        <source>&amp;Exit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="224"/>
        <source>Close the application.</source>
        <translation>Fermer l&apos;application.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="227"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="229"/>
        <source>Quests</source>
        <translation>&amp;Quêtes</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="229"/>
        <source>Save all modified quests to disk.</source>
        <translation>Sauvegarde toutes les quêtes modifiées.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="230"/>
        <source>Dialogs</source>
        <translation>Dialogues</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="230"/>
        <source>Save all modified NPC dialogs.</source>
        <translation>Sauvegarde tous les dialogues des PnJ modifiés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="231"/>
        <source>Archetypes</source>
        <translation>Archétypes</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="231"/>
        <source>Save all modified archetypes.</source>
        <translation>Sauvegarde tous les archétypes modifiés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="232"/>
        <source>Treasures</source>
        <translation>Trésors</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="232"/>
        <source>Save all modified treasures.</source>
        <translation>Sauvegarde tous les trésors modifiés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="233"/>
        <source>General messages</source>
        <translation>Messages généraux</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="233"/>
        <source>Save all modified general messages.</source>
        <translation>Sauvegarde tous les messages généraux modifiés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="234"/>
        <source>Artifacts</source>
        <translation>Artéfacts</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="234"/>
        <source>Save all modified artifacts.</source>
        <translation>Sauvegarde tous les artéfacts modifiés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="236"/>
        <source>&amp;Reports</source>
        <translation>&amp;Rapports</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="237"/>
        <source>Faces and animations report</source>
        <translation>Rapport sur les images et animations</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="237"/>
        <source>Show faces and animations which are used by multiple archetypes, or not used.</source>
        <translation>Affiche les images et animations utilisée par plusieurs archétypes, ou non utilisées.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="238"/>
        <source>Spell damage</source>
        <translation>Dégâts des sorts</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="238"/>
        <source>Display damage by level for some spells.</source>
        <translation>Affiche les dégâts par niveau de certains sorts.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="239"/>
        <source>Alchemy</source>
        <translation>Alchimie</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="239"/>
        <source>Display alchemy formulae, in a table.</source>
        <translation>Affiche les formules d&apos;alchimie, sous forme de tableau.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="240"/>
        <source>Alchemy graph</source>
        <translation>Graphe d&apos;alchimie</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="240"/>
        <source>Export alchemy relationship as a DOT file.</source>
        <translation>Exporte les relations entre formules d&apos;alchimie sous forme de fichier DOT.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="241"/>
        <source>Spells</source>
        <translation>Sorts</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="241"/>
        <source>Display all spells, in a table.</source>
        <translation>Affiche tous les sorts, sous forme de tableau.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="243"/>
        <source>Summoned pets statistics</source>
        <translation>Statistiques des familiers invoqués</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="243"/>
        <source>Display wc, hp, speed and other statistics for summoned pets.</source>
        <translation>Affiche wc, hp, speed et autres statistiques pour les familiers invoqués.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="246"/>
        <source>Materials</source>
        <translation>Matériaux</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="246"/>
        <source>Display all materials with their properties.</source>
        <translation>Affiche tous les matérieux avec leurs propriétés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="248"/>
        <source>Licenses checks</source>
        <translation>Vérifications sur les licences</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="248"/>
        <source>Check for licenses inconsistencies.</source>
        <translation>Cherche des inconsistances de licences.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="249"/>
        <source>Map reset groups</source>
        <translation>Groupes de réinitialisation des cartes</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="249"/>
        <source>List map reset groups.</source>
        <translation>Liste les groupes de réinitialisation des cartes.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="252"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="253"/>
        <source>Edit monsters</source>
        <translation>Éditer les monstres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="253"/>
        <source>Edit monsters in a table.</source>
        <translation>Éditer les monstres via un tableau.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="254"/>
        <source>Monster resistances overview</source>
        <translation>Aperçu des résistances des monstres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="254"/>
        <source>Display an overview of resistances of monsters</source>
        <translation>Affiche un aperçu des résistances des monstres aux attaques élémentaires</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="260"/>
        <source>Generate smooth face base</source>
        <translation>Générer une base d&apos;image lissée</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="260"/>
        <source>Generate the basic smoothed picture for a face.</source>
        <translation>Génère la base d&apos;une image lisse pour une image.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="261"/>
        <source>Generate HP bar</source>
        <translation>Générer une barre de points de vie</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="261"/>
        <source>Generate faces for a HP bar.</source>
        <translation>Génère les images pour une barre de points de vie.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="262"/>
        <source>Combat simulator</source>
        <translation>Simulateur de combat</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="262"/>
        <source>Simulate fighting between two objects.</source>
        <translation>Simule le combat de deux objets.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="263"/>
        <source>Generate face variants</source>
        <translation>Générer des variations d&apos;image</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="263"/>
        <source>Generate faces by changing colors of existing faces.</source>
        <translation>Génère des images en changeant des couleurs d&apos;images existantes.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="265"/>
        <source>Reload assets</source>
        <translation>Recharger les ressources</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="265"/>
        <source>Reload all assets from the data directory.</source>
        <translation>Recharge toutes les ressources depuis le répertoire de données.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="266"/>
        <source>Sounds</source>
        <translation>Sons</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="266"/>
        <source>Display defined sounds and associated files.</source>
        <translation>Affiche les sons et les fichiers associés.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="270"/>
        <source>&amp;Windows</source>
        <translation>&amp;Fenêtres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="272"/>
        <source>Restore windows positions at launch</source>
        <translation>Restaurer la position des fenêtres au démarrage</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="272"/>
        <source>If enabled then opened windows are automatically opened again when the application starts</source>
        <translation>Si activée, les fenêtres ouvertes seront automatiquement réouvertes au démarrage</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="281"/>
        <source>Close current window</source>
        <translation>Fermer la fenêtre courante</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="281"/>
        <source>Close the currently focused window</source>
        <translation>Fermer la fenêtre actuellement active</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="282"/>
        <source>Close all windows</source>
        <translation>Fermer toutes les fenêtres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="282"/>
        <source>Close all opened windows</source>
        <translation>Ferme toutes les fenêtres ouvertes</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="283"/>
        <source>Tile windows</source>
        <translation>Mosaïque de fenêtres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="283"/>
        <source>Tile all windows</source>
        <translation>Répartit les fenêtres en mosaïque</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="284"/>
        <source>Cascade windows</source>
        <translation>Cascade de fenêtres</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="284"/>
        <source>Cascade all windows</source>
        <translation>Affiche toutes les fenêtres en cascade</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="290"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="291"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="291"/>
        <source>CRE Help</source>
        <translation>Aide de CRE</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="296"/>
        <source>About</source>
        <translation>&amp;À propos</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="296"/>
        <location filename="../CREMainWindow.cpp" line="298"/>
        <source>About CRE</source>
        <translation>À propos de CRE</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="301"/>
        <source>Show changes after updating</source>
        <translation>Afficher les changements après mise à jour</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="301"/>
        <source>If checked, then show latest changes at first startup after an update</source>
        <translation>Si activée, les modifications après mise à jour seront automatiquement affichées au premier démarrage après cette mise à jour</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="310"/>
        <source>Changes</source>
        <translation>Modifications</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="310"/>
        <source>Display CRE changes</source>
        <translation>Affiche l&apos;historique des modifications de CRE</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="356"/>
        <source>Facesets</source>
        <translation>Assortiments d&apos;images</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="390"/>
        <source>Browsing map %1</source>
        <translation>Analyse de la carte %1</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="395"/>
        <source>Finished browsing maps.</source>
        <translation>Analyse des cartes terminées.</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="617"/>
        <source>%1 [%2]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="765"/>
        <source>&lt;h1&gt;Formulae with chance of 0&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="791"/>
        <source>Destination file</source>
        <translation>Fichier de destination</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="791"/>
        <source>Dot files (*.dot);;All files (*.*)</source>
        <translation>Fichiers dot (*.dot);;Tous les fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="798"/>
        <source>Unable to write to %1</source>
        <translation>Erreur lors de l&apos;écriture de %1</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="824"/>
        <source>%1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="846"/>
        <location filename="../CREMainWindow.cpp" line="851"/>
        <source>alchemy_%1 [label=&quot;%2&quot;]
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="854"/>
        <source>alchemy_%1 -&gt; alchemy_%2
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="868"/>
        <source> (%1 formulae not displayed)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1194"/>
        <source>&lt;h1&gt;Player vs monsters&lt;/h1&gt;&lt;p&gt;&lt;strong&gt;fv&lt;/strong&gt; is the level at which the first victory happened, &lt;strong&gt;hv&lt;/strong&gt; is the level at which at least 50% of fights were victorious.&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1291"/>
        <source>&lt;h1&gt;Summoned pet statistics&lt;/h1&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1406"/>
        <source>&lt;h1&gt;Shop information&lt;/h1&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1539"/>
        <source>%1 completed out of %2 (%3%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1574"/>
        <source>&lt;tr&gt;&lt;td&gt;%1&lt;/td&gt;&lt;td&gt;%2&lt;/td&gt;&lt;/tr&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1580"/>
        <source>&lt;h1&gt;%1&lt;/h1&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1581"/>
        <source>&lt;tr&gt;&lt;th rowspan=&apos;2&apos;&gt;Name&lt;/th&gt;&lt;th colspan=&apos;%1&apos;&gt;%2&lt;/th&gt;&lt;/tr&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1589"/>
        <source>&lt;tr&gt;&lt;td&gt;%1&lt;/td&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1592"/>
        <source>&lt;td&gt;%1&lt;/td&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1641"/>
        <source>&lt;tr&gt;&lt;td&gt;&lt;img src=&apos;data:image/png;base64,%1&apos;&gt;&lt;/td&gt;&lt;td&gt;%2&lt;/td&gt;&lt;td&gt;%3&lt;/td&gt;&lt;td&gt;%4&lt;/td&gt;&lt;/tr&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1642"/>
        <source>unknown: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1773"/>
        <source>&lt;li&gt;%1 (%2)&lt;/li&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1853"/>
        <source>Please select the &apos;sounds&apos; directory</source>
        <translation>Veuillez sélectionner le répertoire &quot;sounds&quot;</translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1881"/>
        <source>&amp;%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CREMainWindow.cpp" line="1883"/>
        <source>%1 %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CREMapPanel</name>
    <message>
        <location filename="../CREMapPanel.cpp" line="47"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../CREMapPanel.cpp" line="52"/>
        <source>Exits from this map</source>
        <translation>Sorties depuis cette carte</translation>
    </message>
    <message>
        <location filename="../CREMapPanel.cpp" line="56"/>
        <source>Exits leading to this map</source>
        <translation>Sorties conduisant à cette carte</translation>
    </message>
    <message>
        <location filename="../CREMapPanel.cpp" line="60"/>
        <source>Scripts on this map</source>
        <translation>Scripts présents sur cette carte</translation>
    </message>
    <message>
        <location filename="../CREMapPanel.cpp" line="64"/>
        <source>Background music:</source>
        <translation>Musique d&apos;ambiance :</translation>
    </message>
</context>
<context>
    <name>CREMessageItemModel</name>
    <message>
        <location filename="../CREMessageItemModel.cpp" line="218"/>
        <source>match</source>
        <translation>correspondance</translation>
    </message>
    <message>
        <location filename="../CREMessageItemModel.cpp" line="220"/>
        <source>pre-conditions</source>
        <translation>pré-conditions</translation>
    </message>
    <message>
        <location filename="../CREMessageItemModel.cpp" line="222"/>
        <source>player suggested replies</source>
        <translation>réponses suggérées au joueur</translation>
    </message>
    <message>
        <location filename="../CREMessageItemModel.cpp" line="224"/>
        <source>NPC messages</source>
        <translation>messages du PnJ</translation>
    </message>
    <message>
        <location filename="../CREMessageItemModel.cpp" line="226"/>
        <source>post-conditions</source>
        <translation>post-conditions</translation>
    </message>
    <message>
        <location filename="../CREMessageItemModel.cpp" line="228"/>
        <source>includes</source>
        <translation>inclusions</translation>
    </message>
</context>
<context>
    <name>CREMessagePanel</name>
    <message>
        <location filename="../CREMessagePanel.cpp" line="30"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="32"/>
        <source>Path:</source>
        <translation>Chemin :</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="33"/>
        <source>Location:</source>
        <translation>Emplacement :</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="53"/>
        <source>Message rules (blue: uses token set by current rule as pre-condition, red: rule that sets token for pre-condition of current rule)</source>
        <translation>Règles du message (bleu : utilise en pré-condition un jeton défini par la règle courante, rouge : une règle qui définit un jeton utilisé en pré-condition de la règle courante)</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="57"/>
        <source>insert rule</source>
        <translation>intérer une règle</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="60"/>
        <source>remove rule</source>
        <translation>supprimer la règle</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="64"/>
        <source>move up</source>
        <translation>déplacer vers le haut</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="67"/>
        <source>move down</source>
        <translation>déplacer vers le bas</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="71"/>
        <source>copy</source>
        <translation>copier</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="75"/>
        <source>reset all changes</source>
        <translation>abandonner tous les changements</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="81"/>
        <location filename="../CREMessagePanel.cpp" line="83"/>
        <source>Use</source>
        <translation>Utilisation</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="112"/>
        <source>Maps</source>
        <translation>Cartes</translation>
    </message>
    <message>
        <location filename="../CREMessagePanel.cpp" line="144"/>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
</context>
<context>
    <name>CREPlayerRepliesPanel</name>
    <message>
        <location filename="../CREPlayerRepliesPanel.cpp" line="67"/>
        <source>Player suggested replies</source>
        <translation>Réponses suggérées au joueur</translation>
    </message>
    <message>
        <location filename="../CREPlayerRepliesPanel.cpp" line="74"/>
        <source>add reply</source>
        <translation>ajouter une réponse</translation>
    </message>
    <message>
        <location filename="../CREPlayerRepliesPanel.cpp" line="77"/>
        <source>delete reply</source>
        <translation>supprimer la réponse</translation>
    </message>
    <message>
        <location filename="../CREPlayerRepliesPanel.cpp" line="80"/>
        <source>reset changes</source>
        <translation>abandonner les modifications</translation>
    </message>
</context>
<context>
    <name>CREPrePostList</name>
    <message>
        <location filename="../CREPrePostList.cpp" line="112"/>
        <source>Message pre-conditions</source>
        <translation>Pré-conditions du message</translation>
    </message>
    <message>
        <location filename="../CREPrePostList.cpp" line="115"/>
        <source>Message post-conditions</source>
        <translation>Post-conditions du message</translation>
    </message>
    <message>
        <location filename="../CREPrePostList.cpp" line="118"/>
        <source>Step set when conditions are met</source>
        <translation>L&apos;étape est définie lorsque ces conditions sont remplies</translation>
    </message>
</context>
<context>
    <name>CREPrePostPanel</name>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="431"/>
        <source>Message pre-condition</source>
        <translation>Pré-condition du message</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="434"/>
        <source>Message post-condition</source>
        <translation>Post-condition du message</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="437"/>
        <source>Step set when</source>
        <translation>Étape définie lorsque</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="443"/>
        <source>Script:</source>
        <translation>Script :</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="463"/>
        <source>reset changes</source>
        <translation>abandonner les modifications</translation>
    </message>
</context>
<context>
    <name>CREQuestPanel</name>
    <message>
        <location filename="../CREQuestPanel.cpp" line="28"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="29"/>
        <source>Code:</source>
        <translation>Code :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="30"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="31"/>
        <source>Face:</source>
        <translation>Image :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="32"/>
        <source>Parent:</source>
        <translation>Parent :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="33"/>
        <source>Can be done multiple times</source>
        <translation>Peut être rejouée</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="34"/>
        <source>System quest:</source>
        <translation>Quête système :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="35"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="36"/>
        <source>Comment:</source>
        <translation>Commentaire :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="39"/>
        <source>Use</source>
        <translation>Utilisation</translation>
    </message>
</context>
<context>
    <name>CRERandomMapPanel</name>
    <message>
        <location filename="../random_maps/RandomMapPanel.cpp" line="21"/>
        <source>Source map:</source>
        <translation>Carte d&apos;origine :</translation>
    </message>
    <message>
        <location filename="../random_maps/RandomMapPanel.cpp" line="23"/>
        <source>Parameters:</source>
        <translation>Paramètres :</translation>
    </message>
</context>
<context>
    <name>CREReportDialog</name>
    <message>
        <location filename="../CREReportDialog.cpp" line="24"/>
        <source>Report parameters</source>
        <translation>Paramètres du rapport</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="31"/>
        <source>Add</source>
        <translation>Ajouter</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="35"/>
        <source>Remove</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="39"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="44"/>
        <source>Header:</source>
        <translation>Entête :</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="48"/>
        <source>Text to display at the top of the report.</source>
        <translation>Texte à afficher au début du rapport.</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="50"/>
        <source>Footer:</source>
        <translation>Pied de page :</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="54"/>
        <source>Text to display at the bottom of the report.</source>
        <translation>Texte à afficher à la fin du rapport.</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="56"/>
        <source>Item sort:</source>
        <translation>Tri des éléments :</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="60"/>
        <source>Expression used to sort items. The items to be compared are &apos;left&apos; and &apos;right&apos;, and the expression should be true if left &lt; right, false else.</source>
        <translation>Expression permettant de trier les éléments. Les éléments à comparer sont « left » et « right », et l&apos;expression doit retourner « true » si left est plus petit que right, « false » sinon.</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="62"/>
        <source>Item display:</source>
        <translation>Affichage de l&apos;élément :</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="66"/>
        <source>Expression used to display one item. The current item is &apos;item&apos;, and the expression should be a string value.</source>
        <translation>Expression utilisée pour afficher un élément. L&apos;élément courant est « item », et l&apos;expression devrait être une valeur chaîne de caractère.</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="90"/>
        <source>Discard changes?</source>
        <translation>Abandonner les modifications ?</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="90"/>
        <source>You are about to discard all changes!
Are you sure?</source>
        <translation>Vous allez abandonner toutes les modifications !
Confirmez-vous ?</translation>
    </message>
    <message>
        <location filename="../CREReportDialog.cpp" line="98"/>
        <source>Report help</source>
        <translation>Aide pour le rapport</translation>
    </message>
</context>
<context>
    <name>CREReportDisplay</name>
    <message>
        <location filename="../CREReportDisplay.cpp" line="32"/>
        <source>Copy (HTML)</source>
        <translation>Copier (format HTML)</translation>
    </message>
    <message>
        <location filename="../CREReportDisplay.cpp" line="36"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>CREResourcesWindow</name>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="95"/>
        <location filename="../CREResourcesWindow.cpp" line="285"/>
        <source>Filter...</source>
        <translation>Filtrer...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="100"/>
        <source>Report</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="133"/>
        <source>No details available.</source>
        <translation>Aucun détail disponible.</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="237"/>
        <source>(none)</source>
        <translation>(aucun)</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="254"/>
        <source>Quick filter...</source>
        <translation>Filtre rapide...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="257"/>
        <source>Filters definition...</source>
        <translation>Définition des filtres...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="285"/>
        <source>Filter: %1</source>
        <translation>Filtre : %1</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="318"/>
        <source>Reports definition...</source>
        <translation>Définition des rapports...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="331"/>
        <source>Generating report...</source>
        <translation>Génération du rapport...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="331"/>
        <source>Abort report</source>
        <translation>Annuler le rapport</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="332"/>
        <location filename="../CREResourcesWindow.cpp" line="436"/>
        <source>Report: &apos;%1&apos;</source>
        <translation>Rapport : « %1 »</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="363"/>
        <source>Sorting items...</source>
        <translation>Tri des éléments...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="394"/>
        <source>Generating items text...</source>
        <translation>Génération du texte des éléments...</translation>
    </message>
    <message>
        <location filename="../CREResourcesWindow.cpp" line="465"/>
        <source>Quest %1 already exists!</source>
        <translation>La quête « %1 » existe déjà !</translation>
    </message>
</context>
<context>
    <name>CREScriptPanel</name>
    <message>
        <location filename="../scripts/ScriptFilePanel.cpp" line="26"/>
        <source>Maps using this script</source>
        <translation>Cartes utilisant ce script</translation>
    </message>
</context>
<context>
    <name>CRESettingsDialog</name>
    <message>
        <location filename="../CRESettingsDialog.cpp" line="19"/>
        <source>CRE settings</source>
        <translation>Paramètres de CRE</translation>
    </message>
    <message>
        <location filename="../CRESettingsDialog.cpp" line="25"/>
        <source>Cache directory:</source>
        <translation>Répertoire de cache :</translation>
    </message>
    <message>
        <location filename="../CRESettingsDialog.cpp" line="34"/>
        <source>This directory is used to store information between program runs.
It should be persistent (don&apos;t put in a temporary directory cleaned now and then),
but data can be recreated if needed.</source>
        <translation>Ce répertoire est utilisé pour stocker des données entre les exécutions de l&apos;application.
Il devrait être persistant (n&apos;utilisez pas un répertoire temporaire nettoyé de temps en temps),
mais les données peuvent être recréées si besoin.</translation>
    </message>
</context>
<context>
    <name>CRESmoothFaceMaker</name>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="34"/>
        <source>Face to use:</source>
        <translation>Image à utiliser :</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="38"/>
        <source>Path of the picture to create:</source>
        <translation>Chemin de l&apos;image à créer :</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="43"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="52"/>
        <source>Smooth face base generator</source>
        <translation>Générateur de base de face lisse</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="80"/>
        <source>Oops</source>
        <translation>Oups</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="80"/>
        <source>You must select a destination!</source>
        <translation>Vous devez choisir une destination !</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="86"/>
        <source>Confirm file overwrite</source>
        <translation>Confirmez l&apos;écrasement du fichier</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="86"/>
        <source>File %1 already exists. Overwrite it?</source>
        <translation>Le fichier %1 existe déjà. L&apos;écraser ?</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="190"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="190"/>
        <source>Error while saving the picture as %1!</source>
        <translation>Erreur lors de la sauvegarde de l&apos;image sous le nom %1 !</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="194"/>
        <source>Smooth base saved</source>
        <translation>Base de face lisse sauvegardée</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="194"/>
        <source>The smooth base was correctly saved as %1</source>
        <translation>La bae de face lisse a été correctment sauvegardée sous le nom %1</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="204"/>
        <source>Select destination file</source>
        <translation>Sélectionnez le fichier de destination</translation>
    </message>
    <message>
        <location filename="../CRESmoothFaceMaker.cpp" line="204"/>
        <source>PNG file (*.png);;All files (*.*)</source>
        <translation>Fichier PNG (*.png);;Tous les fichiers (*.*)</translation>
    </message>
</context>
<context>
    <name>CREStringListPanel</name>
    <message>
        <location filename="../CREStringListPanel.cpp" line="20"/>
        <source>NPC possible messages</source>
        <translation>Messages que le PnJ peut dire</translation>
    </message>
    <message>
        <location filename="../CREStringListPanel.cpp" line="23"/>
        <source>Messages:</source>
        <translation>Messages :</translation>
    </message>
    <message>
        <location filename="../CREStringListPanel.cpp" line="31"/>
        <source>add</source>
        <translation>ajouter</translation>
    </message>
    <message>
        <location filename="../CREStringListPanel.cpp" line="35"/>
        <source>remove</source>
        <translation>supprimer</translation>
    </message>
    <message>
        <location filename="../CREStringListPanel.cpp" line="39"/>
        <source>reset changes</source>
        <translation>abandonner les modifications</translation>
    </message>
</context>
<context>
    <name>CRESubItemConnection</name>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="92"/>
        <source>Connection number:</source>
        <translation>Numéro de la connexion :</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="107"/>
        <source>Not enough arguments</source>
        <translation>Pas assez de paramètres</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="115"/>
        <location filename="../CREPrePostPanel.cpp" line="135"/>
        <source>Invalid number %1, must be a number between 1 and 65000</source>
        <translation>« %1 » est un nombre invalide, la connexion doit être comprise entre 1 et 65000</translation>
    </message>
</context>
<context>
    <name>CRESubItemList</name>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="29"/>
        <source>add</source>
        <translation>ajouter</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="33"/>
        <source>delete</source>
        <translation>supprimer</translation>
    </message>
</context>
<context>
    <name>CRESubItemQuest</name>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="149"/>
        <source>Quest:</source>
        <translation>Quête :</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="156"/>
        <source>at step</source>
        <translation>à l&apos;étape</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="158"/>
        <source>below step</source>
        <translation>avant l&apos;étape</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="161"/>
        <source>from step</source>
        <translation>à partir de l&apos;étape</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="164"/>
        <source>from step to step</source>
        <translation>entre les étapes</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="174"/>
        <source>New step:</source>
        <translation>Nouvelle étape :</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="286"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
</context>
<context>
    <name>CRESubItemToken</name>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="356"/>
        <source>Token:</source>
        <translation>Jeton :</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="363"/>
        <source>Values the token can be (one per line):</source>
        <translation>Valeurs que le jeton peut avoir (une par ligne) :</translation>
    </message>
    <message>
        <location filename="../CREPrePostPanel.cpp" line="372"/>
        <source>Value to set for the token:</source>
        <translation>Valeur à définir pour le jeton :</translation>
    </message>
</context>
<context>
    <name>CRETreasurePanel</name>
    <message>
        <location filename="../treasures/TreasureListPanel.cpp" line="30"/>
        <source>Single item</source>
        <translation>Élément unique</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureListPanel.cpp" line="32"/>
        <source>Difficulty:</source>
        <translation>Difficulté :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureListPanel.cpp" line="36"/>
        <source>generate</source>
        <translation>générer</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureListPanel.cpp" line="40"/>
        <source>Generation result</source>
        <translation>Résultat de la génération</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureListPanel.cpp" line="43"/>
        <source>Used by</source>
        <translation>Utilisé par</translation>
    </message>
</context>
<context>
    <name>ChangesDock</name>
    <message>
        <location filename="../ChangesDock.cpp" line="19"/>
        <source>Changes</source>
        <translation>Modifications</translation>
    </message>
</context>
<context>
    <name>EditMonstersDialog</name>
    <message>
        <location filename="../EditMonstersDialog.cpp" line="22"/>
        <source>Edit monsters</source>
        <translation>Éditer les monstres</translation>
    </message>
    <message>
        <location filename="../EditMonstersDialog.cpp" line="29"/>
        <source>Save</source>
        <translation>Sauvegarder</translation>
    </message>
    <message>
        <location filename="../EditMonstersDialog.cpp" line="33"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>FaceMakerDialog</name>
    <message>
        <location filename="../FaceMakerDialog.cpp" line="31"/>
        <source>Settings:</source>
        <translation>Paramètres :</translation>
    </message>
    <message>
        <location filename="../FaceMakerDialog.cpp" line="45"/>
        <source>Face variant maker</source>
        <translation>Générateur de variations d&apos;images</translation>
    </message>
</context>
<context>
    <name>FacesWrapper</name>
    <message>
        <location filename="../faces/FacesWrapper.h" line="23"/>
        <source>Display all faces.</source>
        <translation>Affiche toutes les images.</translation>
    </message>
    <message>
        <location filename="../faces/FacesWrapper.h" line="23"/>
        <source>Faces</source>
        <translation>Images</translation>
    </message>
</context>
<context>
    <name>FacesetsPanel</name>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="23"/>
        <source>Prefix:</source>
        <translation>Préfixe :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="24"/>
        <source>Full name:</source>
        <translation>Nom complet :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="25"/>
        <source>Fallback:</source>
        <translation>Alternative :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="26"/>
        <source>Size:</source>
        <translation>Taille :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="27"/>
        <source>Extension:</source>
        <translation>Extension :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="28"/>
        <source>Defined faces:</source>
        <translation>Images définies :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="29"/>
        <source>License information:</source>
        <translation>Informations de licence :</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="49"/>
        <source>%1 out of %2 (%3%)</source>
        <translation>%1 sur %2 (%3%)</translation>
    </message>
    <message>
        <location filename="../faces/FacesetsPanel.cpp" line="54"/>
        <source>%1 faces have license information out of %2 (%3%)</source>
        <translation>%1 images ont des informations de licence parmi %2 (%3%)</translation>
    </message>
</context>
<context>
    <name>GameSoundPanel</name>
    <message>
        <location filename="../sounds/GameSoundPanel.cpp" line="16"/>
        <source>Game sound:</source>
        <translation>Son du serveur :</translation>
    </message>
    <message>
        <location filename="../sounds/GameSoundPanel.cpp" line="17"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
</context>
<context>
    <name>GameSounds</name>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="19"/>
        <source>player learns a spell</source>
        <translation>le joueur apprend un sort</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="20"/>
        <source>player applies an item</source>
        <translation>le joueur utilise ou équipe un object</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="22"/>
        <source>someone applies an item that is only available to DM; an item is destroyed to save the player&apos;s life</source>
        <translation>quelqu&apos;un utilise un objet qui n&apos;est disponible que pour les Maîtres de Jeu ; un objet est détruit pour sauver la vie du joueur</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="23"/>
        <source>player (or pet) hits a living creature</source>
        <translation>un joueur (ou un familier) cause des dégâts à une créature vivante</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="24"/>
        <source>player fails hitting a living creature</source>
        <translation>le joueur échoue à causer des dégâts à une créature vivante</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="25"/>
        <source>player is killed</source>
        <translation>le joueur est tué</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="26"/>
        <source>player is walking</source>
        <translation>le joueur marche</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="27"/>
        <source>player fires a bow or crossbow</source>
        <translation>le joueur tire à l&apos;arc ou à l&apos;arbalète</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="28"/>
        <source>player attempts to use a rod or wand without enough sp or charges</source>
        <translation>le joueur tente d&apos;utiliser un bâton ou une baguette qui n&apos;a pas assez de points de magie ou de charges</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="29"/>
        <source>player opens a door with a key</source>
        <translation>le joueur ouvre une porte avec une clé</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="30"/>
        <source>a living creature is pushed</source>
        <translation>une créature vivante est poussée</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="31"/>
        <source>player dies</source>
        <translation>le joueur meurt</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="32"/>
        <source>a wand explodes when an attempt to recharged it is made</source>
        <translation>une baguette explose lors d&apos;une tentative de la recharger</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="33"/>
        <source>player fails casting a praying spell; player fails to learn a spell</source>
        <translation>le joueur échoue à lancer un sort divin ; le joueur échoue à apprendre un sort</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="34"/>
        <source>a button is walked on</source>
        <translation>quelqu&apos;un marche sur un bouton</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="35"/>
        <source>a handle is applied</source>
        <translation>quelqu&apos;un actionne un levier</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="36"/>
        <source>player applies a clock</source>
        <translation>le joueur utilise une pendule</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="37"/>
        <source>someone drinks an item</source>
        <translation>quelqu&apos;un boit quelque chose</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="38"/>
        <source>a gate is opening or closing</source>
        <translation>une porte s&apos;ouvre ou se ferme</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="39"/>
        <source>someone falls in a hole</source>
        <translation>quelqu&apos;un tombe dans un trou</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="40"/>
        <source>player applies a poisoned food or drink</source>
        <translation>le joueur mange ou boit un aliment empoisonné</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="44"/>
        <source>Display all game internal sounds.</source>
        <translation>Affiche tous les sons du serveur.</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.cpp" line="55"/>
        <source>All sound names defined in the server and sent to the client</source>
        <translation>Tous les noms de sons définis dans le code du serveur et envoyés au client</translation>
    </message>
    <message>
        <location filename="../sounds/GameSounds.h" line="28"/>
        <source>Game sounds</source>
        <translation>Sons du serveur</translation>
    </message>
</context>
<context>
    <name>GeneralMessageWrapper</name>
    <message>
        <location filename="../general_messages/GeneralMessageWrapper.h" line="41"/>
        <source>General message</source>
        <translation>Message général</translation>
    </message>
</context>
<context>
    <name>GeneralMessagesWrapper</name>
    <message>
        <location filename="../general_messages/GeneralMessagesWrapper.h" line="30"/>
        <source>Display all general messages.</source>
        <translation>Affiche tous les messages généraux.</translation>
    </message>
    <message>
        <location filename="../general_messages/GeneralMessagesWrapper.h" line="30"/>
        <source>General messages</source>
        <translation>Messages généraux</translation>
    </message>
</context>
<context>
    <name>HelpManager</name>
    <message>
        <location filename="../HelpManager.cpp" line="33"/>
        <source>Contents</source>
        <translation>Contenu</translation>
    </message>
    <message>
        <location filename="../HelpManager.cpp" line="34"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
</context>
<context>
    <name>MessageManager</name>
    <message>
        <location filename="../MessageManager.cpp" line="24"/>
        <source>Display all NPC dialogs.</source>
        <translation>Affiche tous les dialogues des PnJ.</translation>
    </message>
    <message>
        <location filename="../MessageManager.h" line="31"/>
        <source>NPC dialogs</source>
        <translation>Dialogues des PnJ</translation>
    </message>
</context>
<context>
    <name>MonsterResistances</name>
    <message>
        <location filename="../MonsterResistances.cpp" line="39"/>
        <source>Monster resistances</source>
        <translation>Résistances des monstres</translation>
    </message>
    <message>
        <location filename="../MonsterResistances.cpp" line="44"/>
        <source>Monster resistances (by steps of 10):</source>
        <translation>Résistances des monstres (par pas de 10) :</translation>
    </message>
    <message>
        <location filename="../MonsterResistances.cpp" line="87"/>
        <source>%1% %2: %3 monsters</source>
        <translation>%1% %2 : %3 monstres</translation>
    </message>
</context>
<context>
    <name>PrePostWidget</name>
    <message>
        <location filename="../CREPrePostList.cpp" line="23"/>
        <source>add</source>
        <translation>ajouter</translation>
    </message>
    <message>
        <location filename="../CREPrePostList.cpp" line="26"/>
        <source>delete</source>
        <translation>supprimer</translation>
    </message>
    <message>
        <location filename="../CREPrePostList.cpp" line="29"/>
        <source>reset changes</source>
        <translation>abandonner les modifications</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../cre.cpp" line="45"/>
        <source>Initializing CRE...</source>
        <translation>Initialisation de CRE...</translation>
    </message>
</context>
<context>
    <name>QTreeWidget</name>
    <message>
        <location filename="../CREUtils.cpp" line="46"/>
        <source>Faces</source>
        <translation>Images</translation>
    </message>
</context>
<context>
    <name>QuestStepPanel</name>
    <message>
        <location filename="../CREQuestPanel.cpp" line="18"/>
        <source>Step:</source>
        <translation>Étape :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="19"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="20"/>
        <source>End of quest:</source>
        <translation>Fin de quête :</translation>
    </message>
    <message>
        <location filename="../CREQuestPanel.cpp" line="21"/>
        <source>Conditions</source>
        <translation>Conditions</translation>
    </message>
</context>
<context>
    <name>QuestStepWrapper</name>
    <message>
        <location filename="../quests/QuestWrapper.cpp" line="20"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../quests/QuestWrapper.h" line="38"/>
        <source>%1 (%2)%3</source>
        <translation>%1 (%2)%3</translation>
    </message>
</context>
<context>
    <name>QuestWrapper</name>
    <message>
        <location filename="../quests/QuestWrapper.cpp" line="141"/>
        <source>Add step</source>
        <translation>Ajouter une étape</translation>
    </message>
</context>
<context>
    <name>QuestsWrapper</name>
    <message>
        <location filename="../quests/QuestsWrapper.cpp" line="20"/>
        <source>Add quest</source>
        <translation>Ajouter une quête</translation>
    </message>
    <message>
        <location filename="../quests/QuestsWrapper.h" line="26"/>
        <source>Display all quests.</source>
        <translation>Affiche toutes les quêtes.</translation>
    </message>
    <message>
        <location filename="../quests/QuestsWrapper.h" line="26"/>
        <source>Quests</source>
        <translation>Quêtes</translation>
    </message>
</context>
<context>
    <name>QuickFilterDialog</name>
    <message>
        <location filename="../QuickFilterDialog.cpp" line="19"/>
        <source>Quick filter</source>
        <translation>Filtre rapide</translation>
    </message>
    <message>
        <location filename="../QuickFilterDialog.cpp" line="22"/>
        <source>Quick filter:</source>
        <translation>Filtre rapide :</translation>
    </message>
</context>
<context>
    <name>RandomMaps</name>
    <message>
        <location filename="../random_maps/RandomMaps.h" line="25"/>
        <source>Display all random maps.</source>
        <translation>Affiche toutes les cartes aléatoires.</translation>
    </message>
    <message>
        <location filename="../random_maps/RandomMaps.h" line="28"/>
        <source>Random maps</source>
        <translation>Cartes aléatoires</translation>
    </message>
</context>
<context>
    <name>RecipeListWrapper</name>
    <message>
        <location filename="../recipes/RecipeListWrapper.h" line="29"/>
        <source>%1 ingredients</source>
        <translation>%1 ingrédients</translation>
    </message>
</context>
<context>
    <name>RecipePanel</name>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="24"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="25"/>
        <source>Skill:</source>
        <translation>Compétence :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="26"/>
        <source>Cauldron:</source>
        <translation>Chaudron :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="27"/>
        <source>Yield:</source>
        <translation>Rendement :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="28"/>
        <source>Chance:</source>
        <translation>Chance :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="29"/>
        <source>Experience:</source>
        <translation>Expérience :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="30"/>
        <source>Difficulty:</source>
        <translation>Difficulté :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="31"/>
        <source>Transmutation</source>
        <translation>Transmutation</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="32"/>
        <source>Index:</source>
        <translation>Index :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="33"/>
        <source>Ingredients:</source>
        <translation>Ingrédients :</translation>
    </message>
    <message>
        <location filename="../recipes/RecipePanel.cpp" line="36"/>
        <source>Archetypes:</source>
        <translation>Archétypes :</translation>
    </message>
</context>
<context>
    <name>RecipesWrapper</name>
    <message>
        <location filename="../recipes/RecipesWrapper.cpp" line="21"/>
        <source>Display all recipes.</source>
        <translation>Affiche toutes les formules.</translation>
    </message>
    <message>
        <location filename="../recipes/RecipesWrapper.h" line="30"/>
        <source>Recipes</source>
        <translation>Formules</translation>
    </message>
</context>
<context>
    <name>RegionPanel</name>
    <message>
        <location filename="../regions/RegionPanel.cpp" line="18"/>
        <source>Short name:</source>
        <translation>Nom court :</translation>
    </message>
    <message>
        <location filename="../regions/RegionPanel.cpp" line="19"/>
        <source>Long name:</source>
        <translation>Nom long :</translation>
    </message>
    <message>
        <location filename="../regions/RegionPanel.cpp" line="20"/>
        <source>Message:</source>
        <translation>Message :</translation>
    </message>
    <message>
        <location filename="../regions/RegionPanel.cpp" line="21"/>
        <source>Jail:</source>
        <translation>Prison :</translation>
    </message>
</context>
<context>
    <name>RegionsWrapper</name>
    <message>
        <location filename="../regions/RegionsWrapper.cpp" line="23"/>
        <source>Display all maps and regions.</source>
        <translation>Affiche toutes les régions et cartes.</translation>
    </message>
    <message>
        <location filename="../regions/RegionsWrapper.h" line="24"/>
        <source>Regions and maps</source>
        <translation>Régions et cartes</translation>
    </message>
</context>
<context>
    <name>ResourcesManager</name>
    <message>
        <location filename="../ResourcesManager.cpp" line="77"/>
        <source>%1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ResourcesManager.cpp" line="93"/>
        <source>The following errors occurred during asset collection:
</source>
        <translation>Les erreurs suivantes se sont produites durant l&apos;analyse des ressources :
</translation>
    </message>
    <message>
        <location filename="../ResourcesManager.cpp" line="94"/>
        <source>Errors during asset collection!</source>
        <translation>Erreurs durant l&apos;analyse des ressources !</translation>
    </message>
    <message>
        <location filename="../ResourcesManager.cpp" line="246"/>
        <source>Lose changes to treasure list %1?</source>
        <translation>Perdre les modifications apportées à la liste de trésors %1 ?</translation>
    </message>
    <message>
        <location filename="../ResourcesManager.cpp" line="247"/>
        <source>Really discard changes to treasure list %1?</source>
        <translation>Confirmez-vous l&apos;abandon des modifications sur la liste de trésors %1 ?</translation>
    </message>
</context>
<context>
    <name>ScriptFileManager</name>
    <message>
        <location filename="../scripts/ScriptFileManager.cpp" line="18"/>
        <source>Display all Python scripts used in maps.</source>
        <translation>Affiche tous les scripts Python utilisés dans les cartes.</translation>
    </message>
    <message>
        <location filename="../scripts/ScriptFileManager.h" line="33"/>
        <source>Scripts</source>
        <translation>Scripts Python</translation>
    </message>
</context>
<context>
    <name>SoundFilePanel</name>
    <message>
        <location filename="../sounds/SoundFilePanel.cpp" line="16"/>
        <source>Filename:</source>
        <translation>Nom du fichier :</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFilePanel.cpp" line="17"/>
        <source>Used by</source>
        <translation>Utilisé par</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFilePanel.cpp" line="18"/>
        <source>License information</source>
        <translation>Information de licence</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFilePanel.cpp" line="20"/>
        <source>License field</source>
        <translation>Champ de licence</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFilePanel.cpp" line="20"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
</context>
<context>
    <name>SoundFiles</name>
    <message>
        <location filename="../sounds/SoundFiles.cpp" line="23"/>
        <source>Display all sound files.</source>
        <translation>Affiche tous les fichiers sons.</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFiles.cpp" line="61"/>
        <source>Unable to open sound configuration file %1/sounds.conf</source>
        <translation>Erreur lors de l&apos;ouverture du fichier de configuration des sons %1/sounds.conf</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFiles.cpp" line="108"/>
        <source>No sound file found</source>
        <translation>Aucun fichier de son n&apos;a été trouvé</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFiles.cpp" line="120"/>
        <source>%1 sound files with license information on %2 (%3%)</source>
        <translation>%1 fichiers de son avec une information de licence sur %2 (%3%)</translation>
    </message>
    <message>
        <location filename="../sounds/SoundFiles.h" line="27"/>
        <source>Sound files</source>
        <translation>Fichiers son</translation>
    </message>
</context>
<context>
    <name>SoundsDialog</name>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="37"/>
        <source>skill use</source>
        <translation>utilisation de compétence</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="39"/>
        <source>spell cast</source>
        <translation>invocation de sort</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="41"/>
        <source>spell effect</source>
        <translation>effet de sort</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="43"/>
        <source>unknown type</source>
        <translation>type inconnu</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="46"/>
        <source>unknown</source>
        <translation>inconnu</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="51"/>
        <source>Sound information</source>
        <translation>Information sur les sons</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="58"/>
        <source>Event</source>
        <translation>Événement</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="58"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="58"/>
        <source>File</source>
        <translation>Fichier</translation>
    </message>
    <message>
        <location filename="../sounds/SoundsDialog.cpp" line="58"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
</context>
<context>
    <name>TreasurePanel</name>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="23"/>
        <source>Chance:</source>
        <translation>Chance :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="24"/>
        <source>Magic:</source>
        <translation>Magie :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="25"/>
        <source>Count:</source>
        <translation>Nombre :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="26"/>
        <source>Treasure:</source>
        <translation>Trésor :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="27"/>
        <source>Magic to generate with:</source>
        <translation>Magie avec laquelle générer :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="28"/>
        <source>Magic adjustment:</source>
        <translation>Ajustement de la magie :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="29"/>
        <source>Archetype:</source>
        <translation>Archétype :</translation>
    </message>
    <message>
        <location filename="../treasures/TreasurePanel.cpp" line="30"/>
        <source>Artifact:</source>
        <translation>Artéfact :</translation>
    </message>
</context>
<context>
    <name>TreasureWrapper</name>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="45"/>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="56"/>
        <source>%1 (%2%3%, %4 chances on %5)</source>
        <translation>%1 (%2%3%, %4 chances sur %5)</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="58"/>
        <location filename="../treasures/TreasureWrapper.cpp" line="65"/>
        <source>1 to %2, </source>
        <translation>1 à %2, </translation>
    </message>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="63"/>
        <source>%1 (%2%3%)</source>
        <translation>%1 (%2%3%)</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="299"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="301"/>
        <source>Swap &apos;yes&apos; and &apos;no&apos;</source>
        <translation>Échanger « oui » et « non »</translation>
    </message>
</context>
<context>
    <name>TreasureYesNo</name>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="341"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../treasures/TreasureWrapper.cpp" line="342"/>
        <source>Swap &apos;yes&apos; and &apos;no&apos;</source>
        <translation>Échanger « oui » et « non »</translation>
    </message>
</context>
<context>
    <name>TreasuresWrapper</name>
    <message>
        <location filename="../treasures/TreasuresWrapper.h" line="29"/>
        <source>Treasures</source>
        <translation>Trésors</translation>
    </message>
    <message>
        <location filename="../treasures/TreasuresWrapper.h" line="29"/>
        <source>Display all treasures.</source>
        <translation>Affiche tous les trésors.</translation>
    </message>
</context>
</TS>
