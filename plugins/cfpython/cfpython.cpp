/*****************************************************************************/
/* CFPython - A Python module for Crossfire RPG.                             */
/*****************************************************************************/
/* This is the third version of the Crossfire Scripting Engine.              */
/* The first version used Guile. It was directly integrated in the server    */
/* code, but since Guile wasn't perceived as an easy-to-learn, easy-to-use   */
/* language by many, it was dropped in favor of Python.                      */
/* The second version, CFPython 1.0, was included as a plugin and provided   */
/* just about the same level of functionality the current version has. But   */
/* it used a rather counter-intuitive, procedural way of presenting things.  */
/*                                                                           */
/* CFPython 2.0 aims at correcting many of the design flaws crippling the    */
/* older version. It is also the first plugin to be implemented using the    */
/* new interface, that doesn't need awkward stuff like the horrible CFParm   */
/* structure. For the Python writer, things should probably be easier and    */
/* lead to more readable code: instead of writing "CFPython.getObjectXPos(ob)*/
/* he/she now can simply write "ob.X".                                       */
/*                                                                           */
/*****************************************************************************/
/* Please note that it is still very beta - some of the functions may not    */
/* work as expected and could even cause the server to crash.                */
/*****************************************************************************/
/* Version history:                                                          */
/* 0.1  "Ophiuchus"   - Initial Alpha release                                */
/* 0.5  "Stalingrad"  - Message length overflow corrected.                   */
/* 0.6  "Kharkov"     - Message and Write correctly redefined.               */
/* 0.7  "Koursk"      - Setting informations implemented.                    */
/* 1.0a "Petersburg"  - Last "old-fashioned" version, never submitted to CVS.*/
/* 2.0  "Arkangelsk"  - First release of the 2.x series.                     */
/*****************************************************************************/
/* Version: 2.0beta8 (also known as "Alexander")                             */
/* Contact: yann.chachkoff@myrealbox.com                                     */
/*****************************************************************************/
/* That code is placed under the GNU General Public Licence (GPL)            */
/* (C)2001-2005 by Chachkoff Yann (Feel free to deliver your complaints)     */
/*****************************************************************************/
/*  CrossFire, A Multiplayer game for X-windows                              */
/*                                                                           */
/*  Copyright (C) 2000 Mark Wedel                                            */
/*  Copyright (C) 1992 Frank Tore Johansen                                   */
/*                                                                           */
/*  This program is free software; you can redistribute it and/or modify     */
/*  it under the terms of the GNU General Public License as published by     */
/*  the Free Software Foundation; either version 2 of the License, or        */
/*  (at your option) any later version.                                      */
/*                                                                           */
/*  This program is distributed in the hope that it will be useful,          */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/*  GNU General Public License for more details.                             */
/*                                                                           */
/*  You should have received a copy of the GNU General Public License        */
/*  along with this program; if not, write to the Free Software              */
/*  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.                */
/*                                                                           */
/*****************************************************************************/

/* First let's include the header file needed                                */

#include <cfpython.h>
#include <fcntl.h>
#include <stdarg.h>
// node.h is deprecated in python 3.9, and removed in 3.10 due to a new parser for Python.
#ifndef IS_PY3K10
#include <node.h>
#endif
#include <svnversion.h>

CF_PLUGIN char SvnRevPlugin[] = SVN_REV;

#define PYTHON_DEBUG   /**< Give us some general infos out. */
#define PYTHON_CACHE_SIZE 256   /**< Number of python scripts to store the bytecode of at a time. */

/**
 * One compiled script, cached in memory.
 */
struct pycode_cache_entry {
    sstring file;                   /**< Script full path. */
    PyCodeObject *code;             /**< Compiled code, NULL if there was an error. */
    time_t cached_time,             /**< Time this cache entry was created. */
            used_time;              /**< Last use of this cache entry. */
};

#define MAX_COMMANDS    1024
static command_registration registered_commands[MAX_COMMANDS];

/** Cached compiled scripts. */
static pycode_cache_entry pycode_cache[PYTHON_CACHE_SIZE];

static PyObject *CFPythonError;

/** Set up an Python exception object. */
static void set_exception(const char *fmt, ...) {
    char buf[1024];
    va_list arg;

    va_start(arg, fmt);
    vsnprintf(buf, sizeof(buf), fmt, arg);
    va_end(arg);

    PyErr_SetString(PyExc_ValueError, buf);
}

CFPContext *context_stack;

CFPContext *current_context;

static PyObject *shared_data = NULL;

static PyObject *private_data = NULL;

static CFPContext *popContext(void);
static void freeContext(CFPContext *context);
static int do_script(CFPContext *context);

static PyObject *registerGEvent(PyObject *self, PyObject *args) {
    int eventcode;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &eventcode))
        return NULL;

    cf_system_register_global_event(eventcode, PLUGIN_NAME, cfpython_globalEventListener);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *unregisterGEvent(PyObject *self, PyObject *args) {
    int eventcode;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &eventcode))
        return NULL;

    cf_system_unregister_global_event(EVENT_TELL, PLUGIN_NAME);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *createCFObject(PyObject *self, PyObject *args) {
    object *op;
    (void)self;
    (void)args;

    op = cf_create_object();

    return Crossfire_Object_wrap(op);
}

static PyObject *createCFObjectByName(PyObject *self, PyObject *args) {
    char *obname;
    object *op;
    (void)self;

    if (!PyArg_ParseTuple(args, "s", &obname))
        return NULL;

    op = cf_create_object_by_name(obname);

    return Crossfire_Object_wrap(op);
}

static PyObject *getCFPythonVersion(PyObject *self, PyObject *args) {
    int i = 2044;
    (void)self;
    (void)args;

    return Py_BuildValue("i", i);
}

static PyObject *getReturnValue(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("i", current_context->returnvalue);
}

static PyObject *setReturnValue(PyObject *self, PyObject *args) {
    int i;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &i))
        return NULL;
    current_context->returnvalue = i;
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *matchString(PyObject *self, PyObject *args) {
    char *premiere;
    char *seconde;
    const char *result;
    (void)self;

    if (!PyArg_ParseTuple(args, "ss", &premiere, &seconde))
        return NULL;

    result = cf_re_cmp(premiere, seconde);
    if (result != NULL)
        return Py_BuildValue("i", 1);
    else
        return Py_BuildValue("i", 0);
}

static PyObject *findPlayer(PyObject *self, PyObject *args) {
    player *foundpl;
    char *txt;
    (void)self;

    if (!PyArg_ParseTuple(args, "s", &txt))
        return NULL;

    foundpl = cf_player_find(txt);

    if (foundpl != NULL)
        return Py_BuildValue("O", Crossfire_Object_wrap(foundpl->ob));
    else {
        Py_INCREF(Py_None);
        return Py_None;
    }
}

static PyObject *readyMap(PyObject *self, PyObject *args) {
    char *mapname;
    mapstruct *map;
    int flags = 0;
    (void)self;

    if (!PyArg_ParseTuple(args, "s|i", &mapname, &flags))
        return NULL;

    map = cf_map_get_map(mapname, flags);

    return Crossfire_Map_wrap(map);
}

static PyObject *createMap(PyObject *self, PyObject *args) {
    int sizex, sizey;
    mapstruct *map;
    (void)self;

    if (!PyArg_ParseTuple(args, "ii", &sizex, &sizey))
        return NULL;

    map = cf_get_empty_map(sizex, sizey);

    return Crossfire_Map_wrap(map);
}

static PyObject *getMapDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(0));
}

static PyObject *getUniqueDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(1));
}

static PyObject *getTempDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(2));
}

static PyObject *getConfigDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(3));
}

static PyObject *getLocalDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(4));
}

static PyObject *getPlayerDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(5));
}

static PyObject *getDataDirectory(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", cf_get_directory(6));
}

static PyObject *getWhoAmI(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    if (!current_context->who) {
        Py_INCREF(Py_None);
        return Py_None;
    }
    Py_INCREF(current_context->who);
    return current_context->who;
}

static PyObject *getWhoIsActivator(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    if (!current_context->activator) {
        Py_INCREF(Py_None);
        return Py_None;
    }
    Py_INCREF(current_context->activator);
    return current_context->activator;
}

static PyObject *getWhoIsThird(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    if (!current_context->third) {
        Py_INCREF(Py_None);
        return Py_None;
    }
    Py_INCREF(current_context->third);
    return current_context->third;
}

static PyObject *getWhatIsMessage(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    if (*current_context->message == '\0')
        return Py_BuildValue("");
    else
        return Py_BuildValue("s", current_context->message);
}

static PyObject *getScriptName(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    return Py_BuildValue("s", current_context->script);
}

static PyObject *getScriptParameters(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    if (!*current_context->options) {
        Py_INCREF(Py_None);
        return Py_None;
    }
    return Py_BuildValue("s", current_context->options);
}

static PyObject *getEvent(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    if (!current_context->event) {
        Py_INCREF(Py_None);
        return Py_None;
    }
    Py_INCREF(current_context->event);
    return current_context->event;
}

static PyObject *getPrivateDictionary(PyObject *self, PyObject *args) {
    PyObject *data;
    (void)self;
    (void)args;

    data = PyDict_GetItemString(private_data, current_context->script);
    if (!data) {
        data = PyDict_New();
        PyDict_SetItemString(private_data, current_context->script, data);
        Py_DECREF(data);
    }
    Py_INCREF(data);
    return data;
}

static PyObject *getSharedDictionary(PyObject *self, PyObject *args) {
    (void)self;
    (void)args;
    Py_INCREF(shared_data);
    return shared_data;
}

static PyObject *getArchetypes(PyObject *self, PyObject *args) {
    PyObject *list;
    std::vector<archetype *> archs;
    (void)self;
    (void)args;

    cf_system_get_archetype_vector(CFAPI_SYSTEM_ARCHETYPES, &archs);
    list = PyList_New(0);
    for (auto arch : archs) {
        PyList_Append(list, Crossfire_Archetype_wrap(arch));
    }
    return list;
}

static PyObject *getPlayers(PyObject *self, PyObject *args) {
    PyObject *list;
    std::vector<object *> players;
    (void)self;
    (void)args;

    cf_system_get_object_vector(CFAPI_SYSTEM_PLAYERS, &players);

    list = PyList_New(0);
    for (auto pl : players) {
        PyList_Append(list, Crossfire_Object_wrap(pl));
    }
    return list;
}

static PyObject *getMaps(PyObject *self, PyObject *args) {
    PyObject *list;
    std::vector<mapstruct *> maps;
    (void)self;
    (void)args;

    cf_system_get_map_vector(CFAPI_SYSTEM_MAPS, &maps);

    list = PyList_New(0);
    for (auto map : maps) {
        PyList_Append(list, Crossfire_Map_wrap(map));
    }
    return list;
}

static PyObject *getParties(PyObject *self, PyObject *args) {
    PyObject *list;
    std::vector<partylist *> parties;
    (void)self;
    (void)args;

    cf_system_get_party_vector(CFAPI_SYSTEM_PARTIES, &parties);
    list = PyList_New(0);
    for (auto party : parties) {
        PyList_Append(list, Crossfire_Party_wrap(party));
    }
    return list;
}

static PyObject *getRegions(PyObject *self, PyObject *args) {
    PyObject *list;
    std::vector<region *> regions;
    (void)self;
    (void)args;

    cf_system_get_region_vector(CFAPI_SYSTEM_REGIONS, &regions);
    list = PyList_New(0);
    for (auto reg : regions) {
        PyList_Append(list, Crossfire_Region_wrap(reg));
   }
   return list;
}

static PyObject *getFriendlyList(PyObject *self, PyObject *args) {
    PyObject *list;
    std::vector<object *> friends;
    (void)self;
    (void)args;

    cf_system_get_object_vector(CFAPI_SYSTEM_FRIENDLY_LIST, &friends);
    list = PyList_New(0);
    for (auto ob : friends) {
        PyList_Append(list, Crossfire_Object_wrap(ob));
   }
   return list;
}

static void python_command_function(object *op, const char *params, const char *script) {
    char buf[1024], path[1024];
    CFPContext *context;

    snprintf(buf, sizeof(buf), "%s.py", cf_get_maps_directory(script, path, sizeof(path)));

    context = static_cast<CFPContext *>(malloc(sizeof(CFPContext)));
    context->message[0] = 0;

    context->who         = Crossfire_Object_wrap(op);
    context->activator   = NULL;
    context->third       = NULL;
    /* We are not running from an event, so set it to NULL to avoid segfaults. */
    context->event       = NULL;
    snprintf(context->script, sizeof(context->script), "%s", buf);
    if (params)
        snprintf(context->options, sizeof(context->options), "%s", params);
    else
        context->options[0] = 0;
    context->returnvalue = 1; /* Default is "command successful" */

    if (!do_script(context)) {
        freeContext(context);
        return;
    }

    context = popContext();
    freeContext(context);
}

static PyObject *registerCommand(PyObject *self, PyObject *args) {
    char *cmdname;
    char *scriptname;
    double cmdspeed;
    int type = COMMAND_TYPE_NORMAL, index;
    (void)self;

    if (!PyArg_ParseTuple(args, "ssd|i", &cmdname, &scriptname, &cmdspeed, &type))
        return NULL;

    if (cmdspeed < 0) {
        set_exception("speed must not be negative");
        return NULL;
    }

    if (type < 0 || type > COMMAND_TYPE_WIZARD) {
        set_exception("type must be between 0 and 2");
        return NULL;
    }

    for (index = 0; index < MAX_COMMANDS; index++) {
        if (registered_commands[index] == 0) {
            break;
        }
    }
    if (index == MAX_COMMANDS) {
        set_exception("too many registered commands");
        return NULL;
    }

    registered_commands[index] = cf_system_register_command_extra(cmdname, scriptname, python_command_function, type, cmdspeed);
    if (registered_commands[index] == 0) {
        set_exception("failed to register command (overriding an existing one with a different type?)");
        return NULL;
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *getTime(PyObject *self, PyObject *args) {
    PyObject *list;
    timeofday_t tod;
    (void)self;
    (void)args;

    cf_get_time(&tod);

    list = PyList_New(0);
    PyList_Append(list, Py_BuildValue("i", tod.year));
    PyList_Append(list, Py_BuildValue("i", tod.month));
    PyList_Append(list, Py_BuildValue("i", tod.day));
    PyList_Append(list, Py_BuildValue("i", tod.hour));
    PyList_Append(list, Py_BuildValue("i", tod.minute));
    PyList_Append(list, Py_BuildValue("i", tod.dayofweek));
    PyList_Append(list, Py_BuildValue("i", tod.weekofmonth));
    PyList_Append(list, Py_BuildValue("i", tod.season));
    PyList_Append(list, Py_BuildValue("i", tod.periodofday));

    return list;
}

static PyObject *destroyTimer(PyObject *self, PyObject *args) {
    int id;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &id))
        return NULL;
    return Py_BuildValue("i", cf_timer_destroy(id));
}

static PyObject *getMapHasBeenLoaded(PyObject *self, PyObject *args) {
    char *name;
    (void)self;

    if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
    return Crossfire_Map_wrap(cf_map_has_been_loaded(name));
}

static PyObject *findFace(PyObject *self, PyObject *args) {
    char *name;
    (void)self;

    if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
    return Py_BuildValue("i", cf_find_face(name, 0));
}

static PyObject *log_message(PyObject *self, PyObject *args) {
    LogLevel level;
    int intLevel;
    char *message;
    (void)self;

    if (!PyArg_ParseTuple(args, "is", &intLevel, &message))
        return NULL;

    switch (intLevel) {
    case llevError:
        level = llevError;
        break;

    case llevInfo:
        level = llevInfo;
        break;

    case llevDebug:
        level = llevDebug;
        break;

    case llevMonster:
        level = llevMonster;
        break;

    default:
        return NULL;
    }
    if ((message != NULL) && (message[strlen(message)] == '\n'))
        cf_log(level, "CFPython: %s", message);
    else
        cf_log(level, "CFPython: %s\n", message);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *findAnimation(PyObject *self, PyObject *args) {
    char *name;
    (void)self;

    if (!PyArg_ParseTuple(args, "s", &name))
        return NULL;
    return Py_BuildValue("i", cf_find_animation(name));
}

static PyObject *getSeasonName(PyObject *self, PyObject *args) {
    int i;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &i))
        return NULL;
    return Py_BuildValue("s", cf_get_season_name(i));
}

static PyObject *getMonthName(PyObject *self, PyObject *args) {
    int i;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &i))
        return NULL;
    return Py_BuildValue("s", cf_get_month_name(i));
}

static PyObject *getWeekdayName(PyObject *self, PyObject *args) {
    int i;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &i))
        return NULL;
    return Py_BuildValue("s", cf_get_weekday_name(i));
}

static PyObject *getPeriodofdayName(PyObject *self, PyObject *args) {
    int i;
    (void)self;

    if (!PyArg_ParseTuple(args, "i", &i))
        return NULL;
    return Py_BuildValue("s", cf_get_periodofday_name(i));
}

static PyObject *addReply(PyObject *self, PyObject *args) {
    char *word, *reply;
    talk_info *talk;
    (void)self;

    if (current_context->talk == NULL) {
        set_exception("not in a dialog context");
        return NULL;
    }
    talk = current_context->talk;

    if (!PyArg_ParseTuple(args, "ss", &word, &reply)) {
        return NULL;
    }

    if (talk->replies_count == MAX_REPLIES) {
        set_exception("too many replies");
        return NULL;
    }

    talk->replies_words[talk->replies_count] = cf_add_string(word);
    talk->replies[talk->replies_count] = cf_add_string(reply);
    talk->replies_count++;
    Py_INCREF(Py_None);
    return Py_None;

}

static PyObject *setPlayerMessage(PyObject *self, PyObject *args) {
    char *message;
    int type = rt_reply;
    (void)self;

    if (current_context->talk == NULL) {
        set_exception("not in a dialog context");
        return NULL;
    }

    if (!PyArg_ParseTuple(args, "s|i", &message, &type)) {
        return NULL;
    }

    if (current_context->talk->message != NULL)
        cf_free_string(current_context->talk->message);
    current_context->talk->message = cf_add_string(message);
    current_context->talk->message_type = static_cast<reply_type>(type);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *npcSay(PyObject *self, PyObject *args) {
    Crossfire_Object *npc = NULL;
    char *message, buf[2048];
    (void)self;

    if (!PyArg_ParseTuple(args, "O!s", &Crossfire_ObjectType, &npc, &message))
        return NULL;

    if (current_context->talk == NULL) {
        set_exception("not in a dialog context");
        return NULL;
    }

    if (current_context->talk->npc_msg_count == MAX_NPC) {
        set_exception("too many NPCs");
        return NULL;
    }

    if (strlen(message) >= sizeof(buf) - 1)
        cf_log(llevError, "CFPython: warning, too long message in npcSay, will be truncated");
    /** @todo fix by wrapping monster_format_say() (or the whole talk structure methods) */
    snprintf(buf, sizeof(buf), "%s says: %s", npc->obj->name, message);

    current_context->talk->npc_msgs[current_context->talk->npc_msg_count] = cf_add_string(buf);
    current_context->talk->npc_msg_count++;

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *costStringFromValue(PyObject *self, PyObject *args) {
    uint64_t value;
    char buf[2048];
    int largest_coin = 0;
    (void)self;

    if (!PyArg_ParseTuple(args, "L|i", &value, &largest_coin))
        return NULL;

    cf_cost_string_from_value(value, largest_coin, buf, sizeof(buf));
    return Py_BuildValue("s", buf);
}

PyMethodDef CFPythonMethods[] = {
    { "WhoAmI",              getWhoAmI,              METH_NOARGS,  NULL },
    { "WhoIsActivator",      getWhoIsActivator,      METH_NOARGS,  NULL },
    { "WhoIsOther",          getWhoIsThird,          METH_NOARGS,  NULL },
    { "WhatIsMessage",       getWhatIsMessage,       METH_NOARGS,  NULL },
    { "ScriptName",          getScriptName,          METH_NOARGS,  NULL },
    { "ScriptParameters",    getScriptParameters,    METH_NOARGS,  NULL },
    { "WhatIsEvent",         getEvent,               METH_NOARGS,  NULL },
    { "MapDirectory",        getMapDirectory,        METH_NOARGS,  NULL },
    { "UniqueDirectory",     getUniqueDirectory,     METH_NOARGS,  NULL },
    { "TempDirectory",       getTempDirectory,       METH_NOARGS,  NULL },
    { "ConfigDirectory",     getConfigDirectory,     METH_NOARGS,  NULL },
    { "LocalDirectory",      getLocalDirectory,      METH_NOARGS,  NULL },
    { "PlayerDirectory",     getPlayerDirectory,     METH_NOARGS,  NULL },
    { "DataDirectory",       getDataDirectory,       METH_NOARGS,  NULL },
    { "ReadyMap",            readyMap,               METH_VARARGS, NULL },
    { "CreateMap",           createMap,              METH_VARARGS, NULL },
    { "FindPlayer",          findPlayer,             METH_VARARGS, NULL },
    { "MatchString",         matchString,            METH_VARARGS, NULL },
    { "GetReturnValue",      getReturnValue,         METH_NOARGS,  NULL },
    { "SetReturnValue",      setReturnValue,         METH_VARARGS, NULL },
    { "PluginVersion",       getCFPythonVersion,     METH_NOARGS,  NULL },
    { "CreateObject",        createCFObject,         METH_NOARGS,  NULL },
    { "CreateObjectByName",  createCFObjectByName,   METH_VARARGS, NULL },
    { "GetPrivateDictionary", getPrivateDictionary,  METH_NOARGS,  NULL },
    { "GetSharedDictionary", getSharedDictionary,    METH_NOARGS,  NULL },
    { "GetPlayers",          getPlayers,             METH_NOARGS,  NULL },
    { "GetArchetypes",       getArchetypes,          METH_NOARGS,  NULL },
    { "GetMaps",             getMaps,                METH_NOARGS,  NULL },
    { "GetParties",          getParties,             METH_NOARGS,  NULL },
    { "GetRegions",          getRegions,             METH_NOARGS,  NULL },
    { "GetFriendlyList",     getFriendlyList,        METH_NOARGS,  NULL },
    { "RegisterCommand",     registerCommand,        METH_VARARGS, NULL },
    { "RegisterGlobalEvent", registerGEvent,         METH_VARARGS, NULL },
    { "UnregisterGlobalEvent", unregisterGEvent,     METH_VARARGS, NULL },
    { "GetTime",             getTime,                METH_NOARGS,  NULL },
    { "DestroyTimer",        destroyTimer,           METH_VARARGS, NULL },
    { "MapHasBeenLoaded",    getMapHasBeenLoaded,    METH_VARARGS, NULL },
    { "Log",                 log_message,            METH_VARARGS, NULL },
    { "FindFace",            findFace,               METH_VARARGS, NULL },
    { "FindAnimation",       findAnimation,          METH_VARARGS, NULL },
    { "GetSeasonName",       getSeasonName,          METH_VARARGS, NULL },
    { "GetMonthName",        getMonthName,           METH_VARARGS, NULL },
    { "GetWeekdayName",      getWeekdayName,         METH_VARARGS, NULL },
    { "GetPeriodofdayName",  getPeriodofdayName,     METH_VARARGS, NULL },
    { "AddReply",            addReply,               METH_VARARGS, NULL },
    { "SetPlayerMessage",    setPlayerMessage,       METH_VARARGS, NULL },
    { "NPCSay",              npcSay,                 METH_VARARGS, NULL },
    { "CostStringFromValue", costStringFromValue,    METH_VARARGS, NULL },
    { NULL, NULL, 0, NULL }
};

static void initContextStack(void) {
    current_context = NULL;
    context_stack = NULL;
}

static void pushContext(CFPContext *context) {
    if (current_context == NULL) {
        context_stack = context;
        context->down = NULL;
    } else {
        context->down = current_context;
    }
    current_context = context;
}

static CFPContext *popContext(void) {
    CFPContext *oldcontext;

    if (current_context != NULL) {
        oldcontext = current_context;
        current_context = current_context->down;
        return oldcontext;
    }
    else
        return NULL;
}

static void freeContext(CFPContext *context) {
    Py_XDECREF(context->event);
    Py_XDECREF(context->third);
    Py_XDECREF(context->who);
    Py_XDECREF(context->activator);
    free(context);
}

/**
 * Open a file in the way we need it for compilePython() and postInitPlugin().
 */
static PyObject* cfpython_openpyfile(char *filename) {
    PyObject *scriptfile;
    int fd;
    fd = open(filename, O_RDONLY);
    if (fd == -1)
        return NULL;
    scriptfile = PyFile_FromFd(fd, filename, "r", -1, NULL, NULL, NULL, 1);
    return scriptfile;
}

/**
 * Return a file object from a Python file (as needed for compilePython() and
 * postInitPlugin())
 */
static FILE* cfpython_pyfile_asfile(PyObject* obj) {
    return fdopen(PyObject_AsFileDescriptor(obj), "r");
}

/**
 * A Python object receiving the contents of Python's stderr, and used to output
 * to the Crossfire log instead of stderr.
 */
static PyObject *catcher = NULL;

#ifdef IS_PY3K10
/**
 * A Python object so that we only need to import the io module once.
 * The recommended way to load a file to compile in 3.10 and later involves importing the io module, so we really just do that once.
 */
static PyObject *io_module = NULL;
#endif

/**
 * Trace a Python error to the Crossfire log.
 * This uses code from:
 * http://stackoverflow.com/questions/4307187/how-to-catch-python-stdout-in-c-code
 * See also in initPlugin() the parts about stdOutErr.
 */
static void log_python_error(void) {

    PyErr_Print();

    if (catcher != NULL) {
        PyObject *output = PyObject_GetAttrString(catcher, "value"); //get the stdout and stderr from our catchOutErr object
        PyObject* empty = PyUnicode_FromString("");

        cf_log_plain(llevError, PyUnicode_AsUTF8(output));
        Py_DECREF(output);

        PyObject_SetAttrString(catcher, "value", empty);
        Py_DECREF(empty);
    }

    return;
}


/** Outputs the compiled bytecode for a given python file, using in-memory caching of bytecode */
static PyCodeObject *compilePython(char *filename) {
    PyObject *scriptfile = NULL;
    sstring sh_path;
    struct stat stat_buf;
    struct _node *n;
    int i;
    pycode_cache_entry *replace = NULL, *run = NULL;

    if (stat(filename, &stat_buf)) {
        cf_log(llevError, "CFPython: script file %s can't be stat'ed\n", filename);
        return NULL;
    }

    sh_path = cf_add_string(filename);

    /* Search through cache. Four cases:
     * 1) script in cache, but older than file  -> replace cached
     * 2) script in cache and up to date        -> use cached
     * 3) script not in cache, cache not full   -> add to end of cache
     * 4) script not in cache, cache full       -> replace least recently used
     */
    for (i = 0; i < PYTHON_CACHE_SIZE; i++) {
        if (pycode_cache[i].file == NULL) {  /* script not in cache, cache not full */
            replace = &pycode_cache[i];     /* add to end of cache */
            break;
        } else if (pycode_cache[i].file == sh_path) {
            /* script in cache */
            if (pycode_cache[i].code == NULL || (pycode_cache[i].cached_time < stat_buf.st_mtime)) {
                /* cache older than file, replace cached */
                replace = &pycode_cache[i];
            } else {
                /* cache uptodate, use cached*/
                replace = NULL;
                run = &pycode_cache[i];
            }
            break;
        } else if (replace == NULL || pycode_cache[i].used_time < replace->used_time)
            /* if we haven't found it yet, set replace to the oldest cache */
            replace = &pycode_cache[i];
    }

    /* replace a specific cache index with the file */
    if (replace) {
        Py_XDECREF(replace->code); /* safe to call on NULL */
        replace->code = NULL;

        /* Need to replace path string? */
        if (replace->file != sh_path) {
            if (replace->file) {
                cf_free_string(replace->file);
            }
            replace->file = cf_add_string(sh_path);
        }
#ifdef IS_PY3K10
        /* With the new parser in 3.10, we need to read the file contents into a buffer, and then pass that string to compile it.
         * The new parser removes the PyNode functions as well as PyParser_SimpleParseFile,
         * so the code needed to be completely rewritten to work.
         *
         * Python's solution to these changes is to import the io module and use Python's read method to read in the file,
         * and then convert the bytes object into a c-string for Py_CompileString
         *
         * Though, if it is more performant than the previous code, Py_CompileString is
         * available for all Python 3, so it is possible to simplify all of them to this if we need to.
         */
        if (!io_module)
            io_module = PyImport_ImportModule("io");
        scriptfile = PyObject_CallMethod(io_module, "open", "ss", filename, "rb");
        if (!scriptfile) {
            cf_log(llevDebug, "CFPython: script file %s can't be opened\n", filename);
            cf_free_string(sh_path);
            return NULL;
        }
        PyObject *source_bytes = PyObject_CallMethod(scriptfile, "read", "");
        (void)PyObject_CallMethod(scriptfile, "close", "");
        PyObject *code = Py_CompileString(PyBytes_AsString(source_bytes), filename, Py_file_input);
        if (code) {
            replace->code = (PyCodeObject *)code;
        }
        if (PyErr_Occurred())
            log_python_error();
        else
            replace->cached_time = stat_buf.st_mtime;
        run = replace;
#else
        /* Load, parse and compile. Note: because Pyhon may have been built with a
         * different library than Crossfire, the FILE* it uses may be incompatible.
         * Therefore we use PyFile to open the file, then convert to FILE* and get
         * Python's own structure. Messy, but can't be helped...  */
        if (!(scriptfile = cfpython_openpyfile(filename))) {
            cf_log(llevDebug, "CFPython: script file %s can't be opened\n", filename);
            cf_free_string(sh_path);
            return NULL;
        } else {
            /* Note: FILE* being opaque, it works, but the actual structure may be different! */
            FILE* pyfile = cfpython_pyfile_asfile(scriptfile);
            if ((n = PyParser_SimpleParseFile(pyfile, filename, Py_file_input))) {
                replace->code = PyNode_Compile(n, filename);
                PyNode_Free(n);
            }
            if (PyErr_Occurred())
                log_python_error();
            else
                replace->cached_time = stat_buf.st_mtime;
            run = replace;
        }
#endif
    }

    cf_free_string(sh_path);

    if (scriptfile) {
        Py_DECREF(scriptfile);
    }

    assert(run != NULL);
    run->used_time = time(NULL);
    return run->code;
}

static int do_script(CFPContext *context) {
    PyCodeObject *pycode;
    PyObject *dict;
    PyObject *ret;

    cf_log(llevDebug, "CFPython: running script %s\n", context->script);

    pycode = compilePython(context->script);
    if (pycode) {
        pushContext(context);
        dict = PyDict_New();
        PyDict_SetItemString(dict, "__builtins__", PyEval_GetBuiltins());
        ret = PyEval_EvalCode((PyObject *)pycode, dict, NULL);
        if (PyErr_Occurred()) {
            log_python_error();
        }
        Py_XDECREF(ret);
        Py_DECREF(dict);
        return 1;
    } else
        return 0;
}

/**
 * Add constants and a reverse dictionary to get the name from the value.
 * The reverse dictionary will be named '{name}Name'.
 * @param module where to add the constants to.
 * @param name enum name containing the constants.
 * @param constants constants to add, the last item must have its values all NULL.
 */
static void addConstants(PyObject *module, const char *name, const CFConstant *constants) {
    int i = 0;
    char tmp[1024];
    PyObject *cst;
    PyObject *dict;

    snprintf(tmp, sizeof(tmp), "Crossfire_%s", name);

    cst = PyModule_New(tmp);
    dict = PyDict_New();

    while (constants[i].name != NULL) {
        PyModule_AddIntConstant(cst, (char *)constants[i].name, constants[i].value);
        PyDict_SetItem(dict, PyLong_FromLong(constants[i].value), PyUnicode_FromString(constants[i].name));
        i++;
    }
    PyDict_SetItemString(PyModule_GetDict(module), name, cst);

    snprintf(tmp, sizeof(tmp), "%sName", name);
    PyDict_SetItemString(PyModule_GetDict(module), tmp, dict);
    Py_DECREF(dict);
}

/**
 * Do half the job of addConstants. It only
 * sets constants, but not a hashtable to get constant
 * names from values. To be used for collections of constants
 * which are not unique but still are useful for scripts.
 * @param module where to add the constants to.
 * @param name enum name containing the constants.
 * @param constants constants to add, the last item must have its values all NULL.
 */
static void addSimpleConstants(PyObject *module, const char *name, const CFConstant *constants) {
    int i = 0;
    char tmp[1024];
    PyObject *cst;

    snprintf(tmp, sizeof(tmp), "Crossfire_%s", name);

    cst = PyModule_New(tmp);

    while (constants[i].name != NULL) {
        PyModule_AddIntConstant(cst, (char *)constants[i].name, constants[i].value);
        i++;
    }
    PyDict_SetItemString(PyModule_GetDict(module), name, cst);
}

const CFConstant cstDirection[] = {
    { "NORTH", 1 },
    { "NORTHEAST", 2 },
    { "EAST", 3 },
    { "SOUTHEAST", 4 },
    { "SOUTH", 5 },
    { "SOUTHWEST", 6 },
    { "WEST", 7 },
    { "NORTHWEST", 8 },
    { NULL, 0 }
};

const CFConstant cstType[] = {
    { "PLAYER", PLAYER },
    { "TRANSPORT", TRANSPORT },
    { "ROD", ROD },
    { "TREASURE", TREASURE },
    { "POTION", POTION },
    { "FOOD", FOOD },
    { "POISON", POISON },
    { "BOOK", BOOK },
    { "CLOCK", CLOCK },
    { "DRAGON_FOCUS", DRAGON_FOCUS },
    { "ARROW", ARROW },
    { "BOW", BOW },
    { "WEAPON", WEAPON },
    { "ARMOUR", ARMOUR },
    { "PEDESTAL", PEDESTAL },
    { "ALTAR", ALTAR },
    { "LOCKED_DOOR", LOCKED_DOOR },
    { "SPECIAL_KEY", SPECIAL_KEY },
    { "MAP", MAP },
    { "DOOR", DOOR },
    { "KEY", KEY },
    { "TIMED_GATE", TIMED_GATE },
    { "TRIGGER", TRIGGER },
    { "GRIMREAPER", GRIMREAPER },
    { "MAGIC_EAR", MAGIC_EAR },
    { "TRIGGER_BUTTON", TRIGGER_BUTTON },
    { "TRIGGER_ALTAR", TRIGGER_ALTAR },
    { "TRIGGER_PEDESTAL", TRIGGER_PEDESTAL },
    { "SHIELD", SHIELD },
    { "HELMET", HELMET },
    { "MONEY", MONEY },
    { "CLASS", CLASS },
    { "AMULET", AMULET },
    { "PLAYERMOVER", PLAYERMOVER },
    { "TELEPORTER", TELEPORTER },
    { "CREATOR", CREATOR },
    { "SKILL", SKILL },
    { "EARTHWALL", EARTHWALL },
    { "GOLEM", GOLEM },
    { "THROWN_OBJ", THROWN_OBJ },
    { "BLINDNESS", BLINDNESS },
    { "GOD", GOD },
    { "DETECTOR", DETECTOR },
    { "TRIGGER_MARKER", TRIGGER_MARKER },
    { "DEAD_OBJECT", DEAD_OBJECT },
    { "DRINK", DRINK },
    { "MARKER", MARKER },
    { "HOLY_ALTAR", HOLY_ALTAR },
    { "PLAYER_CHANGER", PLAYER_CHANGER },
    { "BATTLEGROUND", BATTLEGROUND },
    { "PEACEMAKER", PEACEMAKER },
    { "GEM", GEM },
    { "FIREWALL", FIREWALL },
    { "CHECK_INV", CHECK_INV },
    { "MOOD_FLOOR", MOOD_FLOOR },
    { "EXIT", EXIT },
    { "ENCOUNTER", ENCOUNTER },
    { "SHOP_FLOOR", SHOP_FLOOR },
    { "SHOP_MAT", SHOP_MAT },
    { "RING", RING },
    { "FLOOR", FLOOR },
    { "FLESH", FLESH },
    { "INORGANIC", INORGANIC },
    { "SKILL_TOOL", SKILL_TOOL },
    { "LIGHTER", LIGHTER },
    { "WALL", WALL },
    { "MISC_OBJECT", MISC_OBJECT },
    { "MONSTER", MONSTER },
    { "LAMP", LAMP },
    { "DUPLICATOR", DUPLICATOR },
    { "SPELLBOOK", SPELLBOOK },
    { "CLOAK", CLOAK },
    { "SPINNER", SPINNER },
    { "GATE", GATE },
    { "BUTTON", BUTTON },
    { "CF_HANDLE", CF_HANDLE },
    { "HOLE", HOLE },
    { "TRAPDOOR", TRAPDOOR },
    { "SIGN", SIGN },
    { "BOOTS", BOOTS },
    { "GLOVES", GLOVES },
    { "SPELL", SPELL },
    { "SPELL_EFFECT", SPELL_EFFECT },
    { "CONVERTER", CONVERTER },
    { "BRACERS", BRACERS },
    { "POISONING", POISONING },
    { "SAVEBED", SAVEBED },
    { "WAND", WAND },
    { "SCROLL", SCROLL },
    { "DIRECTOR", DIRECTOR },
    { "GIRDLE", GIRDLE },
    { "FORCE", FORCE },
    { "POTION_RESIST_EFFECT", POTION_RESIST_EFFECT },
    { "EVENT_CONNECTOR", EVENT_CONNECTOR },
    { "CLOSE_CON", CLOSE_CON },
    { "CONTAINER", CONTAINER },
    { "ARMOUR_IMPROVER", ARMOUR_IMPROVER },
    { "WEAPON_IMPROVER", WEAPON_IMPROVER },
    { "SKILLSCROLL", SKILLSCROLL },
    { "DEEP_SWAMP", DEEP_SWAMP },
    { "IDENTIFY_ALTAR", IDENTIFY_ALTAR },
    { "SHOP_INVENTORY", SHOP_INVENTORY },
    { "RUNE", RUNE },
    { "TRAP", TRAP },
    { "POWER_CRYSTAL", POWER_CRYSTAL },
    { "CORPSE", CORPSE },
    { "DISEASE", DISEASE },
    { "SYMPTOM", SYMPTOM },
    { "BUILDER", BUILDER },
    { "MATERIAL", MATERIAL },
    { "MIMIC", MIMIC },
    { "LIGHTABLE", LIGHTABLE },
    { NULL, 0 }
};

const CFConstant cstMove[] = {
    { "WALK", MOVE_WALK },
    { "FLY_LOW", MOVE_FLY_LOW },
    { "FLY_HIGH", MOVE_FLY_HIGH },
    { "FLYING", MOVE_FLYING },
    { "SWIM", MOVE_SWIM },
    { "BOAT", MOVE_BOAT },
    { "ALL", MOVE_ALL },
    { NULL, 0 }
};

const CFConstant cstMessageFlag[] = {
    { "NDI_BLACK", NDI_BLACK },
    { "NDI_WHITE", NDI_WHITE },
    { "NDI_NAVY", NDI_NAVY },
    { "NDI_RED", NDI_RED },
    { "NDI_ORANGE", NDI_ORANGE },
    { "NDI_BLUE", NDI_BLUE },
    { "NDI_DK_ORANGE", NDI_DK_ORANGE },
    { "NDI_GREEN", NDI_GREEN },
    { "NDI_LT_GREEN", NDI_LT_GREEN },
    { "NDI_GREY", NDI_GREY },
    { "NDI_BROWN", NDI_BROWN },
    { "NDI_GOLD", NDI_GOLD },
    { "NDI_TAN", NDI_TAN },
    { "NDI_UNIQUE", NDI_UNIQUE },
    { "NDI_ALL", NDI_ALL },
    { "NDI_ALL_DMS", NDI_ALL_DMS },
    { NULL, 0 }
};

const CFConstant cstAttackType[] = {
    { "PHYSICAL", AT_PHYSICAL },
    { "MAGIC", AT_MAGIC },
    { "FIRE", AT_FIRE },
    { "ELECTRICITY", AT_ELECTRICITY },
    { "COLD", AT_COLD },
    { "CONFUSION", AT_CONFUSION },
    { "ACID", AT_ACID },
    { "DRAIN", AT_DRAIN },
    { "WEAPONMAGIC", AT_WEAPONMAGIC },
    { "GHOSTHIT", AT_GHOSTHIT },
    { "POISON", AT_POISON },
    { "SLOW", AT_SLOW },
    { "PARALYZE", AT_PARALYZE },
    { "TURN_UNDEAD", AT_TURN_UNDEAD },
    { "FEAR", AT_FEAR },
    { "CANCELLATION", AT_CANCELLATION },
    { "DEPLETE", AT_DEPLETE },
    { "DEATH", AT_DEATH },
    { "CHAOS", AT_CHAOS },
    { "COUNTERSPELL", AT_COUNTERSPELL },
    { "GODPOWER", AT_GODPOWER },
    { "HOLYWORD", AT_HOLYWORD },
    { "BLIND", AT_BLIND },
    { "INTERNAL", AT_INTERNAL },
    { "LIFE_STEALING", AT_LIFE_STEALING },
    { "DISEASE", AT_DISEASE },
    { NULL, 0 }
};

const CFConstant cstAttackTypeNumber[] = {
    { "PHYSICAL", ATNR_PHYSICAL },
    { "MAGIC", ATNR_MAGIC },
    { "FIRE", ATNR_FIRE },
    { "ELECTRICITY", ATNR_ELECTRICITY },
    { "COLD", ATNR_COLD },
    { "CONFUSION", ATNR_CONFUSION },
    { "ACID", ATNR_ACID },
    { "DRAIN", ATNR_DRAIN },
    { "WEAPONMAGIC", ATNR_WEAPONMAGIC },
    { "GHOSTHIT", ATNR_GHOSTHIT },
    { "POISON", ATNR_POISON },
    { "SLOW", ATNR_SLOW },
    { "PARALYZE", ATNR_PARALYZE },
    { "TURN_UNDEAD", ATNR_TURN_UNDEAD },
    { "FEAR", ATNR_FEAR },
    { "CANCELLATION", ATNR_CANCELLATION },
    { "DEPLETE", ATNR_DEPLETE },
    { "DEATH", ATNR_DEATH },
    { "CHAOS", ATNR_CHAOS },
    { "COUNTERSPELL", ATNR_COUNTERSPELL },
    { "GODPOWER", ATNR_GODPOWER },
    { "HOLYWORD", ATNR_HOLYWORD },
    { "BLIND", ATNR_BLIND },
    { "INTERNAL", ATNR_INTERNAL },
    { "LIFE_STEALING", ATNR_LIFE_STEALING },
    { "DISEASE", ATNR_DISEASE },
    { NULL, 0 }
};

const CFConstant cstEventType[] = {
    /** Object_specific events. */
    { "APPLY", EVENT_APPLY },
    { "ATTACK", EVENT_ATTACKED },
    { "ATTACKS", EVENT_ATTACKS },
    { "BOUGHT", EVENT_BOUGHT },
    { "CLOSE", EVENT_CLOSE },
    { "DEATH", EVENT_DEATH },
    { "DESTROY", EVENT_DESTROY },
    { "DROP", EVENT_DROP },
    { "PICKUP", EVENT_PICKUP },
    { "SAY", EVENT_SAY },
    { "SELLING", EVENT_SELLING },
    { "STOP", EVENT_STOP },
    { "TIME", EVENT_TIME },
    { "THROW", EVENT_THROW },
    { "TRIGGER", EVENT_TRIGGER },
    { "TIMER", EVENT_TIMER },
    { "USER", EVENT_USER },

    /** Global events. */
    { "BORN", EVENT_BORN },
    { "CLOCK", EVENT_CLOCK },
    { "CRASH", EVENT_CRASH },
    { "GKILL", EVENT_GKILL },
    { "KICK", EVENT_KICK },
    { "LOGIN", EVENT_LOGIN },
    { "LOGOUT", EVENT_LOGOUT },
    { "MAPENTER", EVENT_MAPENTER },
    { "MAPLEAVE", EVENT_MAPLEAVE },
    { "MAPLOAD", EVENT_MAPLOAD },
    { "MAPREADY", EVENT_MAPREADY },
    { "MAPRESET", EVENT_MAPRESET },
    { "MAPUNLOAD", EVENT_MAPUNLOAD },
    { "MUZZLE", EVENT_MUZZLE },
    { "PLAYER_DEATH", EVENT_PLAYER_DEATH },
    { "REMOVE", EVENT_REMOVE },
    { "SHOUT", EVENT_SHOUT },
    { "TELL", EVENT_TELL },
    { NULL, 0 }
};

const CFConstant cstTime[] = {
    { "HOURS_PER_DAY", HOURS_PER_DAY },
    { "DAYS_PER_WEEK", DAYS_PER_WEEK },
    { "WEEKS_PER_MONTH", WEEKS_PER_MONTH },
    { "MONTHS_PER_YEAR", MONTHS_PER_YEAR },
    { "SEASONS_PER_YEAR", SEASONS_PER_YEAR },
    { "PERIODS_PER_DAY", PERIODS_PER_DAY },
    { NULL, 0 }
};

const CFConstant cstReplyTypes[] = {
    { "SAY", rt_say },
    { "REPLY", rt_reply },
    { "QUESTION", rt_question },
    { NULL, 0 }
};

const CFConstant cstAttackMovement[] = {
    { "DISTATT", DISTATT },
    { "RUNATT", RUNATT },
    { "HITRUN", HITRUN },
    { "WAITATT", WAITATT },
    { "RUSH", RUSH },
    { "ALLRUN", ALLRUN },
    { "DISTHIT", DISTHIT },
    { "WAIT2", WAIT2 },
    { "PETMOVE", PETMOVE },
    { "CIRCLE1", CIRCLE1 },
    { "CIRCLE2", CIRCLE2 },
    { "PACEH", PACEH },
    { "PACEH2", PACEH2 },
    { "RANDO", RANDO },
    { "RANDO2", RANDO2 },
    { "PACEV", PACEV },
    { "PACEV2", PACEV2 },
    { NULL, 0 }
};

static void initConstants(PyObject *module) {
    addConstants(module, "Direction", cstDirection);
    addConstants(module, "Type", cstType);
    addConstants(module, "Move", cstMove);
    addConstants(module, "MessageFlag", cstMessageFlag);
    addConstants(module, "AttackType", cstAttackType);
    addConstants(module, "AttackTypeNumber", cstAttackTypeNumber);
    addConstants(module, "EventType", cstEventType);
    addSimpleConstants(module, "Time", cstTime);
    addSimpleConstants(module, "ReplyType", cstReplyTypes);
    addSimpleConstants(module, "AttackMovement", cstAttackMovement);
}

/*
 * Set up the main module and handle misc plugin loading stuff and such.
 */

/**
 * Set up the various types (map, object, archetype and so on) as well as some
 * constants, and Crossfire.error.
 */
static void cfpython_init_types(PyObject* m) {
    PyObject *d = PyModule_GetDict(m);

    Crossfire_ObjectType.tp_new = PyType_GenericNew;
    Crossfire_MapType.tp_new    = PyType_GenericNew;
    Crossfire_PlayerType.tp_new = PyType_GenericNew;
    Crossfire_ArchetypeType.tp_new = PyType_GenericNew;
    Crossfire_PartyType.tp_new = PyType_GenericNew;
    Crossfire_RegionType.tp_new = PyType_GenericNew;
    PyType_Ready(&Crossfire_ObjectType);
    PyType_Ready(&Crossfire_MapType);
    PyType_Ready(&Crossfire_PlayerType);
    PyType_Ready(&Crossfire_ArchetypeType);
    PyType_Ready(&Crossfire_PartyType);
    PyType_Ready(&Crossfire_RegionType);

    Py_INCREF(&Crossfire_ObjectType);
    Py_INCREF(&Crossfire_MapType);
    Py_INCREF(&Crossfire_PlayerType);
    Py_INCREF(&Crossfire_ArchetypeType);
    Py_INCREF(&Crossfire_PartyType);
    Py_INCREF(&Crossfire_RegionType);

    PyModule_AddObject(m, "Object", (PyObject *)&Crossfire_ObjectType);
    PyModule_AddObject(m, "Map", (PyObject *)&Crossfire_MapType);
    PyModule_AddObject(m, "Player", (PyObject *)&Crossfire_PlayerType);
    PyModule_AddObject(m, "Archetype", (PyObject *)&Crossfire_ArchetypeType);
    PyModule_AddObject(m, "Party", (PyObject *)&Crossfire_PartyType);
    PyModule_AddObject(m, "Region", (PyObject *)&Crossfire_RegionType);

    PyModule_AddObject(m, "LogError", Py_BuildValue("i", llevError));
    PyModule_AddObject(m, "LogInfo", Py_BuildValue("i", llevInfo));
    PyModule_AddObject(m, "LogDebug", Py_BuildValue("i", llevDebug));
    PyModule_AddObject(m, "LogMonster", Py_BuildValue("i", llevMonster));

    CFPythonError = PyErr_NewException("Crossfire.error", NULL, NULL);
    PyDict_SetItemString(d, "error", CFPythonError);
}

extern PyObject* PyInit_cjson(void);

static PyModuleDef CrossfireModule = {
    PyModuleDef_HEAD_INIT,
    "Crossfire",       /* m_name     */
    NULL,              /* m_doc      */
    -1,                /* m_size     */
    CFPythonMethods,   /* m_methods  */
    NULL,              /* m_reload   */
    NULL,              /* m_traverse */
    NULL,              /* m_clear    */
    NULL               /* m_free     */
};

static PyObject* PyInit_Crossfire(void)
{
    PyObject *m = PyModule_Create(&CrossfireModule);
    Py_INCREF(m);
    return m;
}

extern "C"
int initPlugin(const char *iversion, f_plug_api gethooksptr) {
    PyObject *m;
    /* Python code to redirect stdouts/stderr. */
    const char *stdOutErr =
"import sys\n\
class CatchOutErr:\n\
    def __init__(self):\n\
        self.value = ''\n\
    def write(self, txt):\n\
        self.value += txt\n\
catchOutErr = CatchOutErr()\n\
sys.stdout = catchOutErr\n\
sys.stderr = catchOutErr\n\
";
    (void)iversion;

    for (int c = 0; c < MAX_COMMANDS; c++) {
        registered_commands[c] = 0;
    }

    cf_init_plugin(gethooksptr);
    cf_log(llevDebug, "CFPython 2.0a init\n");

    PyImport_AppendInittab("Crossfire", &PyInit_Crossfire);
    PyImport_AppendInittab("cjson", &PyInit_cjson);

    Py_Initialize();

    m = PyImport_ImportModule("Crossfire");

    cfpython_init_types(m);

    initConstants(m);
    private_data = PyDict_New();
    shared_data = PyDict_New();

    /* Redirect Python's stderr to a special object so it can be put to
     * the Crossfire log. */
    m = PyImport_AddModule("__main__");
    PyRun_SimpleString(stdOutErr);
    catcher = PyObject_GetAttrString(m, "catchOutErr");
    return 0;
}

CF_PLUGIN void *getPluginProperty(int *type, ...) {
    va_list args;
    const char *propname;
    int size;
    char *buf;

    va_start(args, type);
    propname = va_arg(args, const char *);
    if (!strcmp(propname, "Identification")) {
        buf = va_arg(args, char *);
        size = va_arg(args, int);
        va_end(args);
        snprintf(buf, size, PLUGIN_NAME);
        return NULL;
    } else if (!strcmp(propname, "FullName")) {
        buf = va_arg(args, char *);
        size = va_arg(args, int);
        va_end(args);
        snprintf(buf, size, PLUGIN_VERSION);
        return NULL;
    }
    va_end(args);
    return NULL;
}

static int GECodes[] = {
    EVENT_BORN,
    EVENT_CLOCK,
    EVENT_PLAYER_DEATH,
    EVENT_GKILL,
    EVENT_LOGIN,
    EVENT_LOGOUT,
    EVENT_MAPENTER,
    EVENT_MAPLEAVE,
    EVENT_MAPRESET,
    EVENT_REMOVE,
    EVENT_SHOUT,
    EVENT_TELL,
    EVENT_MUZZLE,
    EVENT_KICK,
    EVENT_MAPUNLOAD,
    EVENT_MAPLOAD,
    EVENT_MAPREADY,
    0
};

static const char* GEPaths[] = {
    "born",
    "clock",
    "death",
    "gkill",
    "login",
    "logout",
    "mapenter",
    "mapleave",
    "mapreset",
    "remove",
    "shout",
    "tell",
    "muzzle",
    "kick",
    "mapunload",
    "mapload",
    "mapready",
    NULL
};

/**
 * Clear the list of event files.
 * @param eventFiles list as returned by getEventFiles(), will be invalid
 * after this function call.
 */
static void freeEventFiles(char **eventFiles) {
    assert(eventFiles);
    for (int e = 0; eventFiles[e] != NULL; e++) {
        free(eventFiles[e]);
    }
    free(eventFiles);
}

/**
 * Get the list of script files to run for the specified global event context.
 * @param context event context, must have its options field correctly filled.
 * @return list of event files that must be deleted by calling freeEventFiles().
 */
static char **getEventFiles(CFPContext *context) {
    char **eventFiles = NULL;
    char name[NAME_MAX+1], path[NAME_MAX + 1];

    int allocated = 0, current = 0;
    DIR *dp;
    struct dirent *d;
    struct stat sb;

    snprintf(name, sizeof(name), "python/events/%s/", context->options);
    cf_get_maps_directory(name, path, sizeof(path));

    dp = opendir(path);
    if (dp == NULL) {
        eventFiles = static_cast<char **>(calloc(1, sizeof(eventFiles[0])));
        eventFiles[0] = NULL;
        return eventFiles;
    }

    while ((d = readdir(dp)) != NULL) {
        snprintf(name, sizeof(name), "%s%s", path, d->d_name);
        stat(name, &sb);
        if (S_ISDIR(sb.st_mode)) {
            continue;
        }
        if (strcmp(d->d_name + strlen(d->d_name) - 3, ".py")) {
            continue;
        }

        if (allocated == current) {
            allocated += 10;
            eventFiles = static_cast<char **>(realloc(eventFiles, sizeof(char *) * (allocated + 1)));
            for (int i = current; i < allocated + 1; i++) {
                eventFiles[i] = NULL;
            }
        }
        eventFiles[current] = strdup(name);
        current++;
    }
    (void)closedir(dp);
    return eventFiles;
}

CF_PLUGIN int postInitPlugin(void) {
    PyObject *scriptfile;
    char path[1024];
    int i;

    cf_log(llevDebug, "CFPython 2.0a post init\n");
    initContextStack();
    for (i = 0; GECodes[i] != 0; i++)
        cf_system_register_global_event(GECodes[i], PLUGIN_NAME, cfpython_globalEventListener);

    scriptfile = cfpython_openpyfile(cf_get_maps_directory("python/events/python_init.py", path, sizeof(path)));
    if (scriptfile != NULL) {
        FILE* pyfile = cfpython_pyfile_asfile(scriptfile);
        PyRun_SimpleFile(pyfile, cf_get_maps_directory("python/events/python_init.py", path, sizeof(path)));
        Py_DECREF(scriptfile);
    }

    for (i = 0; i < PYTHON_CACHE_SIZE; i++) {
        pycode_cache[i].code = NULL;
        pycode_cache[i].file = NULL;
        pycode_cache[i].cached_time = 0;
        pycode_cache[i].used_time = 0;
    }

    return 0;
}

static const char *getGlobalEventPath(int code) {
    for (int i = 0; GECodes[i] != 0; i++) {
        if (GECodes[i] == code)
            return GEPaths[i];
    }
    return "";
}

CF_PLUGIN int cfpython_globalEventListener(int *type, ...) {
    va_list args;
    int rv = 0;
    CFPContext *context;
    char *buf;
    player *pl;
    object *op;
    context = static_cast<CFPContext *>(calloc(1, sizeof(CFPContext)));
    char **files;

    va_start(args, type);
    context->event_code = va_arg(args, int);

    context->message[0] = 0;

    rv = context->returnvalue = 0;
    switch (context->event_code) {
    case EVENT_CRASH:
        cf_log(llevDebug, "CFPython: event_crash unimplemented for now\n");
        break;

    case EVENT_BORN:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        break;

    case EVENT_PLAYER_DEATH:
        op = va_arg(args, object *);
        context->who = Crossfire_Object_wrap(op);
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        break;

    case EVENT_GKILL:
    {
        op = va_arg(args, object *);
        object* hitter = va_arg(args, object *);
        context->who = Crossfire_Object_wrap(op);
        context->activator = Crossfire_Object_wrap(hitter);
        break;
    }

    case EVENT_LOGIN:
        pl = va_arg(args, player *);
        context->activator = Crossfire_Object_wrap(pl->ob);
        buf = va_arg(args, char *);
        if (buf != NULL)
            snprintf(context->message, sizeof(context->message), "%s", buf);
        break;

    case EVENT_LOGOUT:
        pl = va_arg(args, player *);
        context->activator = Crossfire_Object_wrap(pl->ob);
        buf = va_arg(args, char *);
        if (buf != NULL)
            snprintf(context->message, sizeof(context->message), "%s", buf);
        break;

    case EVENT_REMOVE:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        break;

    case EVENT_SHOUT:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        buf = va_arg(args, char *);
        if (buf != NULL)
            snprintf(context->message, sizeof(context->message), "%s", buf);
        break;

    case EVENT_MUZZLE:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        buf = va_arg(args, char *);
        if (buf != NULL)
            snprintf(context->message, sizeof(context->message), "%s", buf);
        break;

    case EVENT_KICK:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        buf = va_arg(args, char *);
        if (buf != NULL)
            snprintf(context->message, sizeof(context->message), "%s", buf);
        break;

    case EVENT_MAPENTER:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        context->who = Crossfire_Map_wrap(va_arg(args, mapstruct *));
        break;

    case EVENT_MAPLEAVE:
        op = va_arg(args, object *);
        context->activator = Crossfire_Object_wrap(op);
        context->who = Crossfire_Map_wrap(va_arg(args, mapstruct *));
        break;

    case EVENT_CLOCK:
        break;

    case EVENT_MAPRESET:
        context->who = Crossfire_Map_wrap(va_arg(args, mapstruct *));
        break;

    case EVENT_TELL:
        op = va_arg(args, object *);
        buf = va_arg(args, char *);
        context->activator = Crossfire_Object_wrap(op);
        if (buf != NULL)
            snprintf(context->message, sizeof(context->message), "%s", buf);
        op = va_arg(args, object *);
        context->third = Crossfire_Object_wrap(op);
        break;

    case EVENT_MAPUNLOAD:
        context->who = Crossfire_Map_wrap(va_arg(args, mapstruct *));
        break;

    case EVENT_MAPLOAD:
        context->who = Crossfire_Map_wrap(va_arg(args, mapstruct *));
        break;
    }
    va_end(args);
    context->returnvalue = 0;

    if (context->event_code == EVENT_CLOCK) {
        // Ignore EVENT_CLOCK. It is not being used in maps, but nevertheless
        // runs python_init.py several times per second even while idling.
        freeContext(context);
        return rv;
    }

    snprintf(context->options, sizeof(context->options), "%s", getGlobalEventPath(context->event_code));
    files = getEventFiles(context);
    for (int file = 0; files[file] != NULL; file++)
    {
        CFPContext *copy = static_cast<CFPContext *>(malloc(sizeof(CFPContext)));
        (*copy) = (*context);
        Py_XINCREF(copy->activator);
        Py_XINCREF(copy->event);
        Py_XINCREF(copy->third);
        Py_XINCREF(copy->who);
        strncpy(copy->script, files[file], sizeof(copy->script));

        if (!do_script(copy)) {
            freeContext(copy);
            freeEventFiles(files);
            return rv;
        }

        copy = popContext();
        rv = copy->returnvalue;

        freeContext(copy);
    }
    freeEventFiles(files);

    /* Invalidate freed map wrapper. */
    if (context->event_code == EVENT_MAPUNLOAD)
        Handle_Map_Unload_Hook((Crossfire_Map *)context->who);

    free(context);

    return rv;
}

CF_PLUGIN int eventListener(int *type, ...) {
    int rv = 0;
    va_list args;
    char *buf;
    CFPContext *context;
    object *event;

    context = static_cast<CFPContext *>(malloc(sizeof(CFPContext)));

    context->message[0] = 0;

    va_start(args, type);

    context->who         = Crossfire_Object_wrap(va_arg(args, object *));
    context->activator   = Crossfire_Object_wrap(va_arg(args, object *));
    context->third       = Crossfire_Object_wrap(va_arg(args, object *));
    buf = va_arg(args, char *);
    if (buf != NULL)
        snprintf(context->message, sizeof(context->message), "%s", buf);
    /* fix = */va_arg(args, int);
    event = va_arg(args, object *);
    context->talk = va_arg(args, talk_info *);
    context->event_code  = event->subtype;
    context->event       = Crossfire_Object_wrap(event);
    cf_get_maps_directory(event->slaying, context->script, sizeof(context->script));
    snprintf(context->options, sizeof(context->options), "%s", event->name);
    context->returnvalue = 0;

    va_end(args);

    if (!do_script(context)) {
        freeContext(context);
        return rv;
    }

    context = popContext();
    rv = context->returnvalue;
    freeContext(context);
    return rv;
}

CF_PLUGIN int   closePlugin(void) {
    int i;

    cf_log(llevDebug, "CFPython 2.0a closing\n");

    for (int c = 0; c < MAX_COMMANDS; c++) {
        if (registered_commands[c]) {
            cf_system_unregister_command(registered_commands[c]);
        }
    }

    for (i = 0; i < PYTHON_CACHE_SIZE; i++) {
        Py_XDECREF(pycode_cache[i].code);
        if (pycode_cache[i].file != NULL)
            cf_free_string(pycode_cache[i].file);
    }

    Py_Finalize();

    return 0;
}
